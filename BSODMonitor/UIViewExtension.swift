//
//  UIViewExtension.swift
//  BSODMonitor
//
//  Created by Михаил on 15/12/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import Foundation
import UIKit

let activityIndicator = UIActivityIndicatorView()

extension UIView {
    func dropShadow(cornerRadius: CGFloat = 10,
                    shadowOffset: CGSize = .zero,
                    shadowRadius: CGFloat = 10,
                    opacity: Float = 0.8,
                    color: UIColor = UIColor(red: 0.97, green: 0.93, blue: 0.19, alpha: 1),
                    fillColor: UIColor = .white) {

        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        shadowLayer.fillColor = fillColor.cgColor
        shadowLayer.shadowColor = color.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = shadowOffset
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius

        layer.insertSublayer(shadowLayer, at: 0)
    }

    func showActivityIndicator() {
        activityIndicator.center = center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
    }

}
