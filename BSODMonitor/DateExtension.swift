//
//  DateExtension.swift
//  BSODMonitor
//
//  Created by Михаил on 24/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation
import Localize_Swift

extension Date {

    func offsetFrom(date: Date) -> String {

        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: self, to: date)

        let minutes = "\(difference.minute ?? 0)"
        let hours = "\(difference.hour ?? 0)"

        return "(in \(hours) hrs \(minutes) mins)"
    }
    
    func timeToInterval(date: Date) -> String {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: self, to: date)
        
        let minutes = 0 - (difference.minute ?? 0)
        let hours = 0 - (difference.hour ?? 0)
        let minutesSuffix = Int(String(minutes).suffix(1))
        
        var fullString = ""
        var minutesString = ""
        
        if Localize.currentLanguage() == "ru" {
            if (minutes < 10) || (minutes > 20){
                if minutesSuffix == 1 {
                    minutesString = "\(minutes) минута назад"
                } else if (minutesSuffix == 2) || (minutesSuffix == 3) || (minutesSuffix == 4) {
                    minutesString = "\(minutes) минуты назад"
                } else {
                    minutesString = "\(minutes) минут назад"
                }
            } else {
                minutesString = "\(minutes) минут назад"
            }
            fullString = "\(hours) ч " + minutesString
        } else if Localize.currentLanguage() == "de" {
            minutesString = "vor \(minutes) minuten"
            fullString = "vor \(hours) h \(minutes) minuten"
        } else if Localize.currentLanguage() == "fr" {
            minutesString = "il y a \(minutes) minutes"
            fullString = "il y a  \(hours) h \(minutes) minutes"
        } else {
            if minutes == 1 {
                minutesString = "\(minutes) minute ago"
            } else {
                minutesString = "\(minutes) minutes ago"
            }
            fullString = "\(hours) h " + minutesString
        }
        return difference.hour != 0 ? fullString : minutesString
    }

}
