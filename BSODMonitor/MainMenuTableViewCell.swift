//
//  MenuTableViewCell.swift
//  BSODMonitor
//
//  Created by Владислав on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

class MainMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

//    override func awakeFromNib() {
//        super.awakeFromNib()
//        self.backgroundColor = Theme.currentTheme.menuBackgroundColor
//    }

}
