//
//  SupportPage.swift
//  BSODMonitor
//
//  Created by Михаил on 27/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class SupportPageViewController: UIViewController {

    @IBOutlet weak var aboutTextLabel: UILabel!

    @IBOutlet weak var headerNameLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var batImg: UIImageView!
    @IBOutlet weak var logoBsodImg: UIImageView!
    @IBOutlet weak var supportIcon: UIImageView!
    @IBOutlet weak var angryBatImg: UIImageView!
    @IBOutlet weak var headerImg: UIImageView!
    @IBOutlet weak var topLine: UIView!
    @IBAction func backButtonTappedAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func batTapped(_ sender: UITapGestureRecognizer) {
        BatActionHelper().batActions()
        angryBatImg.isHidden = !angryBat
    }
    
    @IBAction func openTutorialAction(_ sender: UITapGestureRecognizer) {
        let swipingStoryboard = UIStoryboard(name: "Tutorial", bundle: nil)
        let swipingController = swipingStoryboard.instantiateViewController(withIdentifier: "TutorialViewController")

        navigationController?.pushViewController(swipingController, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        changecolors()
        angryBatImage()
        headerNameLabel.text = "BSOD_HELP".localized()
        aboutTextLabel.text =
"""
        BSOD Monitor:
        ver. \(ConfigHelper.Application.versionNumber)(\(ConfigHelper.Application.buildNumber))

        about
        Donate :)
"""
        NotificationCenter.default.addObserver(self, selector: #selector(changecolors), name: ThemeChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(angryBatImage), name: BatChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func angryBatImage() {
        angryBatImg.isHidden = !angryBat
    }

    @objc func changecolors() {
        backView.backgroundColor = Theme.currentTheme.backgroundColor
        headerImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "header_dark")! : UIImage(named: "header_light")!
        batImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_dark")! : UIImage(named: "bat_light")!
        supportIcon.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "Icon Support Help Dark")! : UIImage(named: "Icon Support Help Light")!
        logoBsodImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "LogoBSODDark")! : UIImage(named: "LogoBSODLight")!
        aboutTextLabel.textColor = Theme.currentTheme.textColor
        self.view.backgroundColor = Theme.currentTheme.backgroundColor
        angryBatImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_angry_dark")! : UIImage(named: "bat_angry_light")!
        headerNameLabel.textColor = Theme.currentTheme.headerTextLabelColor
    }
}
