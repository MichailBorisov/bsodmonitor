//
//  ThemeColorChangeHelper.swift
//  BSODMonitor
//
//  Created by Владислав on 23/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

enum AssetsColor {
    case backgroundDark
    case backgroundLight
    case textColorDark
    case textColorLight
}

extension UIColor {

    static func appColor(_ name: AssetsColor) -> UIColor? {
        switch name {
        case .backgroundDark: return UIColor(named: "backgroundDark")
        case .backgroundLight: return UIColor(named: "backgroundLight")
        case .textColorDark: return UIColor(named: "textColorDark")
        case .textColorLight: return UIColor(named: "textColorLight")
        }
    }

}
