//
//  ConfigHelper.swift
//  BSODMonitor
//
//  Created by Михаил on 06/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class ConfigHelper {

    struct Application {

        enum InfoPlistKeys: String {
            case versionNumber = "CFBundleShortVersionString"
            case buildNumber = "CFBundleVersion"
            case applicationName = "CFBundleDisplayName"
        }

        static var applicationName: String {
            return getInfoFromInfoPlist(by: .applicationName)!
        }

        static var buildNumber: String {
            return getInfoFromInfoPlist(by: .buildNumber)!
        }

        static var versionNumber: String {
            return getInfoFromInfoPlist(by: .versionNumber)!
        }

        static func getInfoFromInfoPlist(by key: InfoPlistKeys) -> String? {
            return Bundle.main.object(forInfoDictionaryKey: key.rawValue) as? String
        }

    }

}
