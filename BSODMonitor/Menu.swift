//
//  Menu.swift
//  BSODMonitor
//
//  Created by Владислав on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import Foundation

struct MainMenu {
    var name: String
    let imageName: String
    let action: () -> Void
}
