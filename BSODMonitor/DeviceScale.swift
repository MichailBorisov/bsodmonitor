//
//  DeviceScale.swift
//  BSODMonitor
//
//  Created by Михаил on 03/03/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

struct TutorialImagesArray {
    let ru: String
    let en: String
    let fr: String
    let de: String
}

struct TutorialImages {
    
    private let lightTutorialFor6or8Phone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 6-8", en: "En Tutorial Page 1 6-8", fr: "Fr Tutorial Page 1 6-8", de: "De Tutorial Page 1 6-8"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 6-8", en: "En Tutorial Page 2 6-8", fr: "Fr Tutorial Page 2 6-8", de: "De Tutorial Page 2 6-8"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 6-8", en: "En Tutorial Page 3 6-8", fr: "Fr Tutorial Page 3 6-8", de: "De Tutorial Page 3 6-8"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 6-8", en: "En Tutorial Page 4 6-8", fr: "Fr Tutorial Page 4 6-8", de: "De Tutorial Page 4 6-8"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 6-8", en: "En Tutorial Page 5 6-8", fr: "Fr Tutorial Page 5 6-8", de: "De Tutorial Page 5 6-8")
    ]
    
    private let darkTutorialFor6or8Phone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 6-8 Dark", en: "En Tutorial Page 1 6-8 Dark", fr: "Fr Tutorial Page 1 6-8 Dark", de: "De Tutorial Page 1 6-8 Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 6-8 Dark", en: "En Tutorial Page 2 6-8 Dark", fr: "Fr Tutorial Page 2 6-8 Dark", de: "De Tutorial Page 2 6-8 Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 6-8 Dark", en: "En Tutorial Page 3 6-8 Dark", fr: "Fr Tutorial Page 3 6-8 Dark", de: "De Tutorial Page 3 6-8 Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 6-8 Dark", en: "En Tutorial Page 4 6-8 Dark", fr: "Fr Tutorial Page 4 6-8 Dark", de: "De Tutorial Page 4 6-8 Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 6-8 Dark", en: "En Tutorial Page 5 6-8 Dark", fr: "Fr Tutorial Page 5 6-8 Dark", de: "De Tutorial Page 5 6-8 Dark")
    ]
    
    private let lightTutorialFor6or8PLUSPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 6-8 PLUS", en: "En Tutorial Page 1 6-8 PLUS", fr: "Fr Tutorial Page 1 6-8 PLUS", de: "De Tutorial Page 1 6-8 PLUS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 6-8 PLUS", en: "En Tutorial Page 2 6-8 PLUS", fr: "Fr Tutorial Page 2 6-8 PLUS", de: "De Tutorial Page 2 6-8 PLUS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 6-8 PLUS", en: "En Tutorial Page 3 6-8 PLUS", fr: "Fr Tutorial Page 3 6-8 PLUS", de: "De Tutorial Page 3 6-8 PLUS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 6-8 PLUS", en: "En Tutorial Page 4 6-8 PLUS", fr: "Fr Tutorial Page 4 6-8 PLUS", de: "De Tutorial Page 4 6-8 PLUS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 6-8 PLUS", en: "En Tutorial Page 5 6-8 PLUS", fr: "Fr Tutorial Page 5 6-8 PLUS", de: "De Tutorial Page 5 6-8 PLUS")
    ]
    
    private let darkTutorialFor6or8PLUSPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 6-8 PLUS Dark", en: "En Tutorial Page 1 6-8 PLUS Dark", fr: "Fr Tutorial Page 1 6-8 PLUS Dark", de: "De Tutorial Page 1 6-8 PLUS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 6-8 PLUS Dark", en: "En Tutorial Page 2 6-8 PLUS Dark", fr: "Fr Tutorial Page 2 6-8 PLUS Dark", de: "De Tutorial Page 2 6-8 PLUS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 6-8 PLUS Dark", en: "En Tutorial Page 3 6-8 PLUS Dark", fr: "Fr Tutorial Page 3 6-8 PLUS Dark", de: "De Tutorial Page 3 6-8 PLUS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 6-8 PLUS Dark", en: "En Tutorial Page 4 6-8 PLUS Dark", fr: "Fr Tutorial Page 4 6-8 PLUS Dark", de: "De Tutorial Page 4 6-8 PLUS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 6-8 PLUS Dark", en: "En Tutorial Page 5 6-8 PLUS Dark", fr: "Fr Tutorial Page 5 6-8 PLUS Dark", de: "De Tutorial Page 5 6-8 PLUS Dark")
    ]
    
    private let lightTutorialForXorXSPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 X-XS", en: "En Tutorial Page 1 X-XS", fr: "Fr Tutorial Page 1 X-XS", de: "De Tutorial Page 1 X-XS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 X-XS", en: "En Tutorial Page 2 X-XS", fr: "Fr Tutorial Page 2 X-XS", de: "De Tutorial Page 2 X-XS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 X-XS", en: "En Tutorial Page 3 X-XS", fr: "Fr Tutorial Page 3 X-XS", de: "De Tutorial Page 3 X-XS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 X-XS", en: "En Tutorial Page 4 X-XS", fr: "Fr Tutorial Page 4 X-XS", de: "De Tutorial Page 4 X-XS"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 X-XS", en: "En Tutorial Page 5 X-XS", fr: "Fr Tutorial Page 5 X-XS", de: "De Tutorial Page 5 X-XS"),
    ]
    
    private let darkTutorialForXorXSPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 X-XS Dark", en: "En Tutorial Page 1 X-XS Dark", fr: "Fr Tutorial Page 1 X-XS Dark", de: "De Tutorial Page 1 X-XS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 X-XS Dark", en: "En Tutorial Page 2 X-XS Dark", fr: "Fr Tutorial Page 2 X-XS Dark", de: "De Tutorial Page 2 X-XS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 X-XS Dark", en: "En Tutorial Page 3 X-XS Dark", fr: "Fr Tutorial Page 3 X-XS Dark", de: "De Tutorial Page 3 X-XS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 X-XS Dark", en: "En Tutorial Page 4 X-XS Dark", fr: "Fr Tutorial Page 4 X-XS Dark", de: "De Tutorial Page 4 X-XS Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 X-XS Dark", en: "En Tutorial Page 5 X-XS Dark", fr: "Fr Tutorial Page 5 X-XS Dark", de: "De Tutorial Page 5 X-XS Dark"),
        ]
    
    private let lightTutorialForXRPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 XR", en: "En Tutorial Page 1 XR", fr: "Fr Tutorial Page 1 XR", de: "De Tutorial Page 1 XR"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 XR", en: "En Tutorial Page 2 XR", fr: "Fr Tutorial Page 2 XR", de: "De Tutorial Page 2 XR"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 XR", en: "En Tutorial Page 3 XR", fr: "Fr Tutorial Page 3 XR", de: "De Tutorial Page 3 XR"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 XR", en: "En Tutorial Page 4 XR", fr: "Fr Tutorial Page 4 XR", de: "De Tutorial Page 4 XR"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 XR", en: "En Tutorial Page 5 XR", fr: "Fr Tutorial Page 5 XR", de: "De Tutorial Page 5 XR")
    ]
    
    private let darkTutorialForXRPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 XR Dark", en: "En Tutorial Page 1 XR Dark", fr: "Fr Tutorial Page 1 XR Dark", de: "De Tutorial Page 1 XR Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 XR Dark", en: "En Tutorial Page 2 XR Dark", fr: "Fr Tutorial Page 2 XR Dark", de: "De Tutorial Page 2 XR Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 XR Dark", en: "En Tutorial Page 3 XR Dark", fr: "Fr Tutorial Page 3 XR Dark", de: "De Tutorial Page 3 XR Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 XR Dark", en: "En Tutorial Page 4 XR Dark", fr: "Fr Tutorial Page 4 XR Dark", de: "De Tutorial Page 4 XR Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 XR Dark", en: "En Tutorial Page 5 XR Dark", fr: "Fr Tutorial Page 5 XR Dark", de: "De Tutorial Page 5 XR Dark")
    ]
    
    private let lightTutorialForXSMaxPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 XSMax", en: "En Tutorial Page 1 XSMax", fr: "Fr Tutorial Page 1 XSMax", de: "De Tutorial Page 1 XSMax"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 XSMax", en: "En Tutorial Page 2 XSMax", fr: "Fr Tutorial Page 2 XSMax", de: "De Tutorial Page 2 XSMax"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 XSMax", en: "En Tutorial Page 3 XSMax", fr: "Fr Tutorial Page 3 XSMax", de: "De Tutorial Page 3 XSMax"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 XSMax", en: "En Tutorial Page 4 XSMax", fr: "Fr Tutorial Page 4 XSMax", de: "De Tutorial Page 4 XSMax"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 XSMax", en: "En Tutorial Page 5 XSMax", fr: "Fr Tutorial Page 5 XSMax", de: "De Tutorial Page 5 XSMax")
    ]
    
    private let darkTutorialForXSMaxPhone: [TutorialImagesArray] = [
        TutorialImagesArray(ru: "Ru Tutorial Page 1 XSMax Dark", en: "En Tutorial Page 1 XSMax Dark", fr: "Fr Tutorial Page 1 XSMax Dark", de: "De Tutorial Page 1 XSMax Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 2 XSMax Dark", en: "En Tutorial Page 2 XSMax Dark", fr: "Fr Tutorial Page 2 XSMax Dark", de: "De Tutorial Page 2 XSMax Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 3 XSMax Dark", en: "En Tutorial Page 3 XSMax Dark", fr: "Fr Tutorial Page 3 XSMax Dark", de: "De Tutorial Page 3 XSMax Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 4 XSMax Dark", en: "En Tutorial Page 4 XSMax Dark", fr: "Fr Tutorial Page 4 XSMax Dark", de: "De Tutorial Page 4 XSMax Dark"),
        TutorialImagesArray(ru: "Ru Tutorial Page 5 XSMax Dark", en: "En Tutorial Page 5 XSMax Dark", fr: "Fr Tutorial Page 5 XSMax Dark", de: "De Tutorial Page 5 XSMax Dark")
    ]
    
    func getImageArray() -> [TutorialImagesArray] {
        switch UIDevice().type {
        case .iPhoneSE ,.iPhone6, .iPhone6S, .iPhone7, .iPhone8:
            return Settings.storage.currentThemeColor == 0 ? lightTutorialFor6or8Phone : darkTutorialFor6or8Phone
        case .iPhone6plus, .iPhone6Splus, .iPhone7plus, .iPhone8plus:
            return Settings.storage.currentThemeColor == 0 ? lightTutorialFor6or8PLUSPhone : darkTutorialFor6or8PLUSPhone
        case .iPhoneX, .iPhoneXS:
            return Settings.storage.currentThemeColor == 0 ? lightTutorialForXorXSPhone : darkTutorialForXorXSPhone
        case .iPhoneXR:
            return Settings.storage.currentThemeColor == 0 ? lightTutorialForXRPhone : darkTutorialForXRPhone
        case .iPhoneXSMax:
            return Settings.storage.currentThemeColor == 0 ? lightTutorialForXSMaxPhone : darkTutorialForXSMaxPhone
        default:
            return Settings.storage.currentThemeColor == 0 ? lightTutorialFor6or8PLUSPhone : darkTutorialFor6or8PLUSPhone
        }
    }
    
}

public enum Model : String {
    case simulator     = "simulator/sandbox",
    //iPod
    iPod1              = "iPod 1",
    iPod2              = "iPod 2",
    iPod3              = "iPod 3",
    iPod4              = "iPod 4",
    iPod5              = "iPod 5",
    //iPad
    iPad2              = "iPad 2",
    iPad3              = "iPad 3",
    iPad4              = "iPad 4",
    iPadAir            = "iPad Air ",
    iPadAir2           = "iPad Air 2",
    iPad5              = "iPad 5", //aka iPad 2017
    iPad6              = "iPad 6", //aka iPad 2018
    //iPad mini
    iPadMini           = "iPad Mini",
    iPadMini2          = "iPad Mini 2",
    iPadMini3          = "iPad Mini 3",
    iPadMini4          = "iPad Mini 4",
    //iPad pro
    iPadPro9_7         = "iPad Pro 9.7\"",
    iPadPro10_5        = "iPad Pro 10.5\"",
    iPadPro12_9        = "iPad Pro 12.9\"",
    iPadPro2_12_9      = "iPad Pro 2 12.9\"",
    //iPhone
    iPhone4            = "iPhone 4",
    iPhone4S           = "iPhone 4S",
    iPhone5            = "iPhone 5",
    iPhone5S           = "iPhone 5S",
    iPhone5C           = "iPhone 5C",
    iPhone6            = "iPhone 6",
    iPhone6plus        = "iPhone 6 Plus",
    iPhone6S           = "iPhone 6S",
    iPhone6Splus       = "iPhone 6S Plus",
    iPhoneSE           = "iPhone SE",
    iPhone7            = "iPhone 7",
    iPhone7plus        = "iPhone 7 Plus",
    iPhone8            = "iPhone 8",
    iPhone8plus        = "iPhone 8 Plus",
    iPhoneX            = "iPhone X",
    iPhoneXS           = "iPhone XS",
    iPhoneXSMax        = "iPhone XS Max",
    iPhoneXR           = "iPhone XR",
    //Apple TV
    AppleTV            = "Apple TV",
    AppleTV_4K         = "Apple TV 4K",
    unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#
//MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
                
            }
        }
        var modelMap : [ String : Model ] = [
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            //iPod
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            //iPad
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPad4,1"   : .iPadAir,
            "iPad4,2"   : .iPadAir,
            "iPad4,3"   : .iPadAir,
            "iPad5,3"   : .iPadAir2,
            "iPad5,4"   : .iPadAir2,
            "iPad6,11"  : .iPad5, //aka iPad 2017
            "iPad6,12"  : .iPad5,
            "iPad7,5"   : .iPad6, //aka iPad 2018
            "iPad7,6"   : .iPad6,
            //iPad mini
            "iPad2,5"   : .iPadMini,
            "iPad2,6"   : .iPadMini,
            "iPad2,7"   : .iPadMini,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad5,1"   : .iPadMini4,
            "iPad5,2"   : .iPadMini4,
            //iPad pro
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7,
            "iPad7,3"   : .iPadPro10_5,
            "iPad7,4"   : .iPadPro10_5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9,
            //iPhone
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPhone7,1" : .iPhone6plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6Splus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,2" : .iPhone7plus,
            "iPhone9,4" : .iPhone7plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,4" : .iPhone8,
            "iPhone10,2" : .iPhone8plus,
            "iPhone10,5" : .iPhone8plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX,
            "iPhone11,2" : .iPhoneXS,
            "iPhone11,4" : .iPhoneXSMax,
            "iPhone11,6" : .iPhoneXSMax,
            "iPhone11,8" : .iPhoneXR,
            //AppleTV
            "AppleTV5,3" : .AppleTV,
            "AppleTV6,2" : .AppleTV_4K
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }
                }
            }
            return model
        }
        return Model.unrecognized
    }
}
