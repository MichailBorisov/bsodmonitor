//
//  EarningsTableCell.swift
//  BSODMonitor
//
//  Created by Владислав on 02/03/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class EarningsTableCell: UITableViewCell {

    //Labels
    @IBOutlet weak var amountNameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var coinNameLabel: UILabel!
    @IBOutlet weak var statusNameLabel: UILabel!
    @IBOutlet weak var coinLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeNameLabel: UILabel!
    @IBOutlet weak var percentNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    
    //Views
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var lineView: UIView!
    
    private func setupTime(data: Last50) {
        if let intDate = data.time {
            let date = Date(timeIntervalSince1970: Double(intDate) ?? 0)
            
            let timeToDate = Date().timeToInterval(date: date)
            
            timeLabel.text = timeToDate
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0))
    }
    
    func setupColors() {
        headerView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        footerView.backgroundColor = Theme.currentTheme.evenTableViewCellsBackground
        lineView.backgroundColor = Theme.currentTheme.footerLineColor
        
        amountNameLabel.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        coinNameLabel.textColor = Theme.currentTheme.homeCellTextColor
        statusNameLabel.textColor = Theme.currentTheme.homeCellTextColor
        timeNameLabel.textColor = Theme.currentTheme.homeCellTextColor
        percentNameLabel.textColor = Theme.currentTheme.homeCellTextColor
        
        amountLabel.textColor = Theme.currentTheme.amountTextColor
        coinLabel.textColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
        statusLabel.textColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
        timeLabel.textColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
        percentLabel.textColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
    }
    
    func setupText() {
        amountNameLabel.text = "AMOUNT".localized() + ":"
        coinNameLabel.text = "COIN_NAME".localized() + ":"
        statusNameLabel.text = "STATUS".localized() + ":"
        timeNameLabel.text = "TIME".localized() + ":"
        percentNameLabel.text = "PERCENT".localized() + ":"
        
        amountNameLabel.font = UIFont(name: "Roboto", size: 14)
        coinNameLabel.font = UIFont(name: "Roboto", size: 12)
        statusNameLabel.font = UIFont(name: "Roboto", size: 12)
        timeNameLabel.font = UIFont(name: "Roboto", size: 12)
        percentNameLabel.font = UIFont(name: "Roboto", size: 12)
        amountLabel.font = UIFont(name: "Roboto", size: 14)
        coinLabel.font = UIFont(name: "Roboto", size: 12)
        statusLabel.font = UIFont(name: "Roboto", size: 12)
        timeLabel.font = UIFont(name: "Roboto", size: 12)
        percentLabel.font = UIFont(name: "Roboto", size: 12)
    }
    
    func setData(entity: Last50) {
        amountLabel.text = "\(entity.amount ?? "")" + " \(entity.currency ?? "")"
        coinLabel.text = "\(entity.currency ?? "")" + " (\(entity.algo ?? ""))"
        statusLabel.text = entity.status
        percentLabel.text = "\(entity.percent ?? "")" + "%"
        setupTime(data: entity)
    }
}
