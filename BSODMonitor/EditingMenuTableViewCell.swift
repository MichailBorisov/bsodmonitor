//
//  EditingMenuTableViewCell.swift
//  BSODMonitor
//
//  Created by Владислав on 30/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

class EditingMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var editingMenuThumbnailImageView: UIImageView!
    @IBOutlet weak var editingMenuNameLabel: UILabel!

}
