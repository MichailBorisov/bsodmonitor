//
//  RealmModel.swift
//  BSODMonitor
//
//  Created by Михаил on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

enum NotificationStatus: String, Codable {
    case on = "active"
    case off = "off"
    case unknown = "unknown"
}

struct WalletModel: Codable {
    var status: NotificationStatus?
    var adress: String?
    var walletName: String?
    var walletInfo: Wallet?
    var customSortIndex: Int = 0
    var isExpanded: Bool = false
}

struct Wallet: Codable {
    let balance: Double?
    let confirmed: Double?
    let currency: String?
    let immature: Double?
    let unsold: Double?
    let unpaid: Double?
    let paid24h: Double?
    let paid: Double?
    let total: Double?
    let miners: [Miners]?
    let last50E: [Last50]?
    let algorithms: [Algorithm]?
    let payouts: [Payout]?
}

struct Miners: Codable {
    let version: String?
    let password: String?
    let ID: String?
    let algo: String?
    let difficulty: Double?
    let hashrate: String?
    let since_last_share: Int?
    let rejected: Double?
}

struct Last50: Codable {
    let algo: String?
    let currency: String?
    let amount: String?
    let percent: String?
    let time: String?
    let status: String?
}

struct Algorithm: Codable {
    let name: String?
    let hashrate: String?
    let symbol: String?
    let miners: Int?
    let shares: String?
}

struct Payout: Codable {
    let time: String?
    let amount: String?
    let tx: String?
}

struct NextPayout: Codable {
    let nextpayout: Int?
}
