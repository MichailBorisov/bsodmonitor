//
//  DarkTheme.swift
//  BSODMonitor
//
//  Created by Владислав on 22/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import UIKit

class DarkTheme: ThemeProtocol {
    var keyboardTheme: UIKeyboardAppearance = .dark
    var backgroundColor: UIColor = #colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1)
    var textColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var sortBackgroundColor: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var arrayImg: UIImage? = UIImage(named: "Icon Sort Arrows Dark")!
    var linesColor: UIColor = #colorLiteral(red: 0.3921568627, green: 0.3921568627, blue: 0.3921568627, alpha: 1)
    var filterTextColor: UIColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
    var filterTextSelected: UIColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
    var linesImage: UIImage? = UIImage(named: "Icon Sort Lines Dark")!
    var backgroundWalletStringColor: UIColor = #colorLiteral(red: 0.01176470588, green: 0.4705882353, blue: 0.5843137255, alpha: 1)
    var menuBackgroundColor: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var addWalletBackgroundColor: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var logoImg: UIImage? = UIImage(named: "LogoBSODDark")!
    var bsodImg: UIImage? = UIImage(named: "menu_bsod_dark")!
    var telegramImg: UIImage?  = UIImage(named: "menu_telegram_dark")!
    var discordImg: UIImage?  = UIImage(named: "menu_discord_dark")!
    var newsImg: UIImage?  = UIImage(named: "menu_news_dark")!
    var settingsImg: UIImage?  = UIImage(named: "menu_settings_dark")!
    var faqImg: UIImage? = UIImage(named: "menu_info_dark")!
    var placeHolderTextColor: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var nextPayoutTextColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var headerTextLabelColor: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    var cancelButtonColor: UIColor = #colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1)
    var walletsNameTextColor: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    var footerBackgroundViewColor: UIColor = #colorLiteral(red: 0.2549019608, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
    var mainHeaderTextColor: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    var mainFooterTextColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var detailsViewBackgroundColor: UIColor = #colorLiteral(red: 0.2549019608, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
    var workersFooterBackgroundViewColor: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var detailsTextColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var workersNameLabelColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var unevenTableViewCellsBackground: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var evenTableViewCellsBackground: UIColor = #colorLiteral(red: 0.2549019608, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
    var homeCellTextColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var amountTextColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var footerLineColor: UIColor = #colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1)
}
