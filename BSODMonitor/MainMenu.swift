//
//  Menu.swift
//  BSODMonitor
//
//  Created by Владислав on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//
import UIKit

struct MainMenu {
    var name: String
    let imageLight: UIImage
    let imageDark: UIImage
    let action: () -> Void
}
