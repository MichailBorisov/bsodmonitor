//
//  APIConstant.swift
//  BSODMonitor
//
//  Created by Михаил on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

struct APIconstant {
    static let url = "http://api.bsod.pw/api/walletfull?address="
    static let nextPayoutUrl = "http://api.bsod.pw/api/nextpayout"
    static let openBSODUrl = "https://bsod.pw/?address="
    static let notificationSubscribe = "http://api.bsod.pw/api/app_subscribe"
}
