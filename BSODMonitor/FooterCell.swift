//
//  FooterCell.swift
//  BSODMonitor
//
//  Created by Михаил on 13/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class FooterCell: UITableViewCell {

    @IBOutlet weak var coinName: UILabel!
    @IBOutlet weak var coinAlgo: UILabel!

    @IBOutlet weak var hashrateNameLabel: UILabel!
    @IBOutlet weak var hashrateValueLabel: UILabel!
    @IBOutlet weak var workersNameLabel: UILabel!
    @IBOutlet weak var workersValueLabel: UILabel!
    @IBOutlet weak var unpaidNameLabel: UILabel!
    @IBOutlet weak var unpaidValueLabel: UILabel!
    @IBOutlet weak var balanceNameLabel: UILabel!
    @IBOutlet weak var balanceValueLabel: UILabel!
    
    @IBOutlet weak var firstFooterView: UIView!
    @IBOutlet weak var secondFooterView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setText()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: LCLLanguageChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setText() {
        hashrateNameLabel.text = "HASHRATE".localized()
        workersNameLabel.text = "WORKERS_NAME".localized()
        unpaidNameLabel.text = "UNPAID".localized()
        balanceNameLabel.text = "BALANCE".localized()
    }

    func setupView(wallet: WalletModel) {
        guard let wallet = wallet.walletInfo else { return }
        coinName.text = wallet.currency ?? "NoN"

        if let algo = wallet.miners?.first?.algo {
            coinAlgo.text = algo
        } else if let algo = wallet.last50E?.first?.algo {
            coinAlgo.text = algo
        }

        if let hashRate = wallet.algorithms?.first?.hashrate {
            hashrateValueLabel.text = "\(hashRate)"
        } else {
            hashrateValueLabel.text = "0 Mh/s"
        }
        workersValueLabel.text = "\(wallet.miners?.count ?? 0)"
        unpaidValueLabel.text = NSString(format: "%.4f", wallet.unpaid ?? 0) as String
        balanceValueLabel.text = NSString(format: "%.4f", wallet.balance ?? 0) as String
    }

}
