//
//  StringExtencion.swift
//  BSODMonitor
//
//  Created by Михаил on 06/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
