//
//  WorkerCell.swift
//  BSODMonitor
//
//  Created by Михаил on 21/02/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class WorkerCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hashrateLabel: UILabel!
    @IBOutlet weak var extraLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var diffLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var hashrateNameLabel: UILabel!
    @IBOutlet weak var extraNameLabel: UILabel!
    @IBOutlet weak var detailsNameLabel: UILabel!
    @IBOutlet weak var diffNameLabel: UILabel!
    
    func setData(entity: Miners) {
        nameLabel.text = entity.ID
        hashrateLabel.text = entity.hashrate
        extraLabel.text = entity.password
        detailLabel.text = entity.version
        diffLabel.text = "\(entity.difficulty ?? 0)"
        
        hashrateNameLabel.text = "HASHRATE".localized() + ":"
        extraNameLabel.text = "EXTRA".localized() + ":"
        detailsNameLabel.text = "DETAILS".localized() + ":"
        diffNameLabel.text = "DIFFICULTY".localized() + ":"
    }
    
    func setColors() {
        headerView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        footerView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        detailsView.backgroundColor = Theme.currentTheme.detailsViewBackgroundColor
        nameLabel.textColor = Theme.currentTheme.mainFooterTextColor
        hashrateNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
        extraNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
        detailsNameLabel.textColor = Theme.currentTheme.detailsTextColor
        diffNameLabel.textColor = Theme.currentTheme.detailsTextColor
    }

}
