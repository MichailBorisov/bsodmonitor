//
//  UIColorExtension.swift
//  BSODMonitor
//
//  Created by Михаил on 13/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
//
import UIKit

class ThemeColor {
    static var defaultBlue: UIColor {
        return UIColor(from255: 0, green: 145, blue: 180)
    }

    static var defaultGray: UIColor {
        return UIColor(from255: 75, green: 75, blue: 75)
    }
    
    static var currentPageIndicatorTintColor: UIColor {
        return UIColor(red: 0, green: 0.57, blue: 0.71, alpha: 1)
    }
    
    static var pageIndicatorTintColor: UIColor {
        return UIColor(red: 0.49, green: 0.49, blue: 0.49, alpha: 1)
    }
}

extension UIColor {

    convenience init(from255 red: CGFloat, green: CGFloat, blue: CGFloat) {
        let const: CGFloat = 255.0
        self.init(red: red/const, green: green/const, blue: blue/const, alpha: 1)
    }

    convenience init(from255 red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let const: CGFloat = 255.0
        self.init(red: red/const, green: green/const, blue: blue/const, alpha: alpha/const)
    }
}
