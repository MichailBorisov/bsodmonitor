//
//  CollectionViewCell.swift
//  BSODMonitor
//
//  Created by Михаил on 21/02/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class WorkersCell: UICollectionViewCell {
    
    var reloadAction: (() -> ())?
    private var workers = [Miners]()
    private let headerId = "headerId"
    private let headerNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.tintColor = ThemeColor.defaultBlue
        refresher.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refresher
    }()
    
    private lazy var collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let sectionInsets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        layout.scrollDirection = .vertical
        layout.sectionInset = sectionInsets
        
        let collectionView = UICollectionView(frame: contentView.frame, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(WorkerCell.nib, forCellWithReuseIdentifier: WorkerCell.identifier)
        collectionView.register(UICollectionViewCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(collectionView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(workers: [Miners]) {
        self.workers = workers
        collectionView.reloadData()
    }
    
    @objc private func refresh() {
        reloadAction?()
        refreshControl.endRefreshing()
    }
}

extension WorkersCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return workers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WorkerCell.identifier, for: indexPath) as? WorkerCell else { return UICollectionViewCell() }
        cell.setColors()
        cell.setData(entity: workers[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView.frame.width - 41) / 2, height: 115)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 22)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath)
        header.backgroundColor = .clear
        header.addSubview(headerNameLabel)
        headerNameLabel.centerXAnchor.constraint(equalTo: header.centerXAnchor).isActive = true
        headerNameLabel.centerYAnchor.constraint(equalTo: header.centerYAnchor).isActive = true
        headerNameLabel.textColor = Theme.currentTheme.workersNameLabelColor
        headerNameLabel.font = UIFont(name: "Roboto", size: 18)
        headerNameLabel.text = "WORKERS_NAME".localized()
        return header
    }
}
