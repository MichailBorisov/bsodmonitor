//
//  EarningsCell.swift
//  BSODMonitor
//
//  Created by Владислав on 02/03/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class EarningsCell: UICollectionViewCell {
    
    var reloadAction: (() -> ())?
    private var last50 = [Last50]()
    
    private lazy var tableView: UITableView = {
        let frame = CGRect(x: contentView.frame.minX + 15, y: contentView.frame.minY, width: contentView.frame.width - 30, height: contentView.frame.height)
        let tableView = UITableView(frame: frame, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(EarningsTableCell.nib, forCellReuseIdentifier: EarningsTableCell.identifier)
        tableView.allowsSelection = false
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        return tableView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.tintColor = ThemeColor.defaultBlue
        refresher.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refresher
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(tableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(last50: [Last50]) {
        self.last50 = last50
        tableView.reloadData()
    }
    
    @objc private func refresh() {
        reloadAction?()
        refreshControl.endRefreshing()
    }
}

extension EarningsCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return last50.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EarningsTableCell.identifier) as? EarningsTableCell else { return UITableViewCell() }
        cell.backgroundColor = Theme.currentTheme.backgroundColor
        cell.setupColors()
        cell.setupText()
        cell.setData(entity: last50[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 22))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 22))
        view.backgroundColor = Theme.currentTheme.backgroundColor
        view.addSubview(label)
        label.text = "LAST50".localized()
        label.textColor = Theme.currentTheme.workersNameLabelColor
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto", size: 18)
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 22
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
