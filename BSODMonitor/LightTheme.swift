//
//  LightTheme.swift
//  BSODMonitor
//
//  Created by Владислав on 22/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import UIKit

class LightTheme: ThemeProtocol {
    var keyboardTheme: UIKeyboardAppearance = .light
    var backgroundColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var textColor: UIColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    var sortBackgroundColor: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    var arrayImg: UIImage? = UIImage(named: "Icon Sort Arrows Light")!
    var linesColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var filterTextColor: UIColor = #colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1)
    var filterTextSelected: UIColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
    var linesImage: UIImage? = UIImage(named: "Icon Sort Lines Light")!
    var backgroundWalletStringColor: UIColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
    var addWalletBackgroundColor: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    var menuBackgroundColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var logoImg: UIImage? = UIImage(named: "LogoBSODLight")!
    var bsodImg: UIImage? = UIImage(named: "menu_bsod_light")!
    var telegramImg: UIImage?  = UIImage(named: "menu_telegram_light")!
    var discordImg: UIImage?  = UIImage(named: "menu_discord_light")!
    var newsImg: UIImage?  = UIImage(named: "menu_news_light")!
    var settingsImg: UIImage?  = UIImage(named: "menu_settings_light")!
    var faqImg: UIImage? = UIImage(named: "menu_info_light")!
    var placeHolderTextColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var nextPayoutTextColor: UIColor = #colorLiteral(red: 0.3921568627, green: 0.3921568627, blue: 0.3921568627, alpha: 1)
    var headerTextLabelColor: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var cancelButtonColor: UIColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    var walletsNameTextColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    var footerBackgroundViewColor: UIColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    var mainHeaderTextColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var mainFooterTextColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var detailsViewBackgroundColor: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    var workersFooterBackgroundViewColor: UIColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    var detailsTextColor: UIColor = #colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1)
    var workersNameLabelColor: UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    var unevenTableViewCellsBackground: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
    var evenTableViewCellsBackground: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    var homeCellTextColor: UIColor = #colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1)
    var amountTextColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    var footerLineColor: UIColor = #colorLiteral(red: 0.8549019608, green: 0.8549019608, blue: 0.8549019608, alpha: 1)
}
