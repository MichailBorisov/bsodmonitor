//
//  BaseRequest.swift
//  BSODMonitor
//
//  Created by Михаил on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import Foundation
import Alamofire
import CommonCrypto
import Localize_Swift

final class SearchRequestBuilder {

    enum Result {
        case success(Wallet)
        case error(Error)
    }
    
    struct WalletError: Codable {
        let error: String
    }

    func search(walletName: String, completion: @escaping (Result) -> Void) {

        guard let url = URL(string: APIconstant.url + walletName) else { fatalError("Dont have url") }

        RequestService().process(url: url, method: .get, parameters: nil, headers: nil) { result in
            switch result {
            case .success(let result):
                do {
                    let searchResult = try JSONDecoder().decode(Wallet.self, from: result)
                    completion(.success(searchResult))
                }
                catch let decodeErr {
                    completion(.error(decodeErr))
                }
            case .failure(var error):
                if error.localizedDescription == "not found or blocked" {
                    error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:"SEARCH_ERROR_DESCRIPTION".localized()])
                } else {
                    error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:"GNP_ERROR_DESCRIPTION".localized()])
                }
                completion(.error(error))
            }
        }
    }

    enum NextPayoutResult {
        case success(NextPayout)
        case error(Error)
    }

    func getNextPayout(completion: @escaping (NextPayoutResult) -> Void) {
        guard let url = URL(string: APIconstant.nextPayoutUrl) else { fatalError("Dont have url") }

        RequestService().process(url: url, method: .get, parameters: nil, headers: nil) { result in
            switch result {
            case .success(let result):
                do {
                    let searchResult = try JSONDecoder().decode(NextPayout.self, from: result)
                    completion(.success(searchResult))
                }
                catch let decodeErr {
                    completion(.error(decodeErr))
                }
            case .failure( _):
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:"GNP_ERROR_DESCRIPTION".localized()])
                completion(.error(error))
            }
        }
    }
    
    func subscribeNotification(adress: String, subscribe: Int, completion: @escaping (Error?) -> Void) {
        guard let url = URL(string: APIconstant.notificationSubscribe) else { fatalError("Dont have url") }
        
        let params: Parameters = [
            "address": adress,
            "token": Settings.storage.token,
            "subscribe": subscribe,
            "check": getCheck(address: adress, token: Settings.storage.token, fcmEnabled: subscribe),
            "lang": Localize.currentLanguage()
        ]
        
        Alamofire.request(url, method: .get, parameters: params, headers: nil).responseData { response in
            if let err = response.result.error {
                completion(err)
                return
            }
            
            if let result = response.result.value {
                do {
                    let searchResult = try JSONDecoder().decode(WalletError.self, from: result)
                    completion(NSError(domain: "BSOD Error", code: 0, userInfo: [NSLocalizedDescriptionKey: searchResult.error]))
                }
                catch {
                    completion(nil)
                }
            }
            else {
                completion(nil)
            }
        }
    }
    
    private func getCheck(address: String, token: String) -> String {
        let str = "\(token)\(address)"
        let md5First = MD5(str)?.lowercased()
        let md5Last = MD5("\(md5First ?? "")black")
        return md5Last?.lowercased() ?? ""
    }
    
    private func getCheck(address: String, token: String, fcmEnabled: Int) -> String {
        let str = "\(token)\(address)\(fcmEnabled)"
        let md5First = MD5(str)?.lowercased()
        let md5Last = MD5("\(md5First ?? "")black")
        return md5Last?.lowercased() ?? ""
    }
    
    private func MD5(_ string: String) -> String? {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(UInt(CC_MD5_DIGEST_LENGTH)))
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> Bool in
            messageData.withUnsafeBytes { messageBytes in
                _ = CC_MD5(messageBytes.baseAddress, CC_LONG(messageData.count), digestBytes.bindMemory(to: UInt8.self).baseAddress)
                return true
            }
        }
        
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
}
