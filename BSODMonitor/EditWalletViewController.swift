//
//  EditWalletViewController.swift
//  BSODMonitor
//
//  Created by Владислав on 01/02/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class EditWalletViewController: UIViewController {
    @IBOutlet weak var textBackgroundView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var bodyBackgroundView: UIView!
    @IBOutlet weak var secondBodyView: UIView!
    @IBOutlet weak var nameBackgroundView: UIView!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var textView: UITextView!

    var wallet: WalletModel?
    var namePlaceholderLabel = UILabel()
    weak var delegate: WalletDetailDelegate?

    @IBAction func closeAction(_ sender: UITapGestureRecognizer) {
        dismissAction()
    }

    @IBAction func agreeButtonPressed(_ sender: UIButton) {
        if let wallet = wallet {
            Settings.storage.changeWalletName(wallet: wallet, newName: textView.text)
            NotificationCenter.default.post(name: notificationWalletName, object: nil)
            dismissAction()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        changeColors()
        setupPlaceholders()

        textView.delegate = self

        bodyLabel.text = wallet?.walletName ?? wallet?.adress
        headerLabel.text = "EDITWALLETNAME".localized()
        
        agreeButton.isEnabled = false

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardChange(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.view.frame.origin = .zero
        } else {
            self.view.frame.origin.y = -keyboardViewEndFrame.height
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func changeColors() {
        bodyBackgroundView.backgroundColor = Theme.currentTheme.addWalletBackgroundColor
        secondBodyView.backgroundColor = Theme.currentTheme.addWalletBackgroundColor
        nameBackgroundView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor

        textView.keyboardAppearance = Theme.currentTheme.keyboardTheme

        bodyLabel.textColor = Theme.currentTheme.filterTextColor
        headerLabel.textColor = Theme.currentTheme.walletsNameTextColor
        textView.backgroundColor = Theme.currentTheme.backgroundColor
        agreeButton.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
    }

    func setupPlaceholders() {
        namePlaceholderLabel.text = "NAME_PLACEHOLDER".localized()
        namePlaceholderLabel.textColor = Theme.currentTheme.placeHolderTextColor
        namePlaceholderLabel.font = UIFont(name: "Roboto", size: 16)
        namePlaceholderLabel.sizeToFit()
        textView.addSubview(namePlaceholderLabel)
        namePlaceholderLabel.frame.origin = CGPoint(x: textView.frame.width / 2 - namePlaceholderLabel.frame.width / 2, y: textView.frame.height / 2 - namePlaceholderLabel.frame.height / 2)
        namePlaceholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func dismissAction() {
        dismiss(animated: true, completion: nil)
        delegate?.removeBlurredBackgroundView()
    }
}

extension EditWalletViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        namePlaceholderLabel.isHidden = !textView.text.isEmpty
        agreeButton.isEnabled = !textView.text.isEmpty
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
