//
//  HomeCell.swift
//  BSODMonitor
//
//  Created by Владислав on 01/03/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {
    
    private var wallet: WalletModel?
    
    var reloadAction: (() -> ())?
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.tintColor = ThemeColor.defaultBlue
        refresher.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refresher
    }()
    
    private lazy var tableView: UITableView = {
        let headerNib = UINib.init(nibName: "HomeTableHeaderView", bundle: Bundle.main)
        let frame = CGRect(x: contentView.frame.minX + 15, y: contentView.frame.minY, width: contentView.frame.width - 30, height: contentView.frame.height)
        let tableView = UITableView(frame: frame, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HomeTableHeaderView")
        tableView.register(HomeTableViewCell.nib, forCellReuseIdentifier: HomeTableViewCell.identifier)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        contentView.addSubview(tableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData(_ wallet: WalletModel) {
        self.wallet = wallet
        tableView.reloadData()
    }
    
    @objc private func refresh() {
        reloadAction?()
        refreshControl.endRefreshing()
    }
}

extension HomeCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wallet?.walletInfo?.payouts?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier) as? HomeTableViewCell else { return UITableViewCell() }
        cell.setup(indexPath: indexPath.row, payout: wallet?.walletInfo?.payouts ?? [])
        cell.payoutAmountLabel.font = UIFont(name: "Roboto", size: 14)
        cell.payoutTimeLabel.font = UIFont(name: "Roboto", size: 14)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HomeTableHeaderView") as! HomeTableHeaderView
        headerView.setColors()
        headerView.setText()
        headerView.setData(entity: wallet ?? WalletModel())
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 215
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
