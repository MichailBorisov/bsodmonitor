//
//  FilterArray.swift
//  BSODMonitor
//
//  Created by Владислав on 29/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

struct FilterArrayModel {
    let ruLang: String
    let enLang: String
    let deLang: String
    let frLang: String
    var isSelected: Bool
    
    init(ruLang: String, enLang: String, deLang: String, frLang: String, isSelected: Bool = false) {
        self.ruLang = ruLang
        self.enLang = enLang
        self.deLang = deLang
        self.frLang = frLang
        self.isSelected = isSelected
    }
}
