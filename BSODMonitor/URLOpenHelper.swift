//
//  URLOpenHelper.swift
//  BSODMonitor
//
//  Created by Владислав on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

final class URLOpenHelper: UIViewController {
    func openURL(_ url: String) {
        let url = URL(string: url)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

}
