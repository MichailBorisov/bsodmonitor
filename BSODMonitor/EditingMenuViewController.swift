//
//  EditingMenuViewController.swift
//  BSODMonitor
//
//  Created by Владислав on 30/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import SafariServices
import Localize_Swift

class EditingMenuViewController: UIViewController {

    @IBOutlet weak var editingMenuVersionLabel: UILabel!
    @IBOutlet weak var editingMenuTableView: UITableView!

    @IBOutlet weak var faqLineView: UIView!
    @IBOutlet weak var helpBackgroundView: UIView!
    @IBOutlet weak var mainBackgroundView: UIView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var editingMenuFaqLabel: UILabel!
    @IBOutlet weak var editingMenuFaqImageView: UIImageView!
    
    let service = SearchRequestBuilder()
    var wallet: WalletModel?
    var editingMenu: [[MainMenu]] = [
        [MainMenu(name: "EDIT_WALLET".localized(), imageLight: UIImage(named: "menu_edit_light")!, imageDark: UIImage(named: "menu_edit_dark")!, action: {
            
        }),
         MainMenu(name: "REFRESH".localized(), imageLight: UIImage(named: "menu_refresh_light")!, imageDark: UIImage(named: "menu_refresh_dark")!, action: {
            print("1")
         }),
         MainMenu(name: "DELETE".localized(), imageLight: UIImage(named: "menu_remove_light")!, imageDark: UIImage(named: "menu_remove_dark")!, action: {
            print("1")
         }),
         MainMenu(name: "OPEN_POOL".localized(), imageLight: UIImage(named: "menu_view_light")!, imageDark: UIImage(named: "menu_view_dark")!, action: {
         })],
        [MainMenu(name: "BSOD_POOL".localized(), imageLight: UIImage(named: "menu_bsod_light")!, imageDark: UIImage(named: "menu_bsod_dark")!, action: {
            URLOpenHelper().openURL("https://bsod.pw/")
        }),
         MainMenu(name: "TELEGRAM".localized(), imageLight: UIImage(named: "menu_telegram_light")!, imageDark: UIImage(named: "menu_telegram_dark")!, action: {
            URLOpenHelper().openURL("https://t.me/bsodpool")
         }),
         MainMenu(name: "DISCORD".localized(), imageLight: UIImage(named: "menu_discord_light")!, imageDark: UIImage(named: "menu_discord_dark")!, action: {
            URLOpenHelper().openURL("https://discord.gg/uNBBfGM")
         })],
        [MainMenu(name: "NEWS".localized(), imageLight: UIImage(named: "menu_news_light")!, imageDark: UIImage(named: "menu_news_dark")!, action: {
            URLOpenHelper().openURL("https://twitter.com/BsodPool")
        })],
        [MainMenu(name: "SETTINGS".localized(), imageLight: UIImage(named: "menu_settings_light")!, imageDark: UIImage(named: "menu_settings_dark")!, action: {
            print("1")
        })]]

    override func viewDidLoad() {
        super.viewDidLoad()
        changeColors()
        fullUI()
        setupTableView()
        setText()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name:LCLLanguageChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeColors), name: ThemeChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func supportPageAction(_ sender: UITapGestureRecognizer) {
        openSupportPage()
    }
    
    @objc func changeColors() {
        editingMenuTableView.backgroundColor = Theme.currentTheme.menuBackgroundColor
        helpBackgroundView.backgroundColor = Theme.currentTheme.menuBackgroundColor
        logoImageView.image = Settings.storage.currentThemeColor == 0 ? UIImage(named: "LogoBSODLight")! : UIImage(named: "LogoBSODDark")!
        faqLineView.backgroundColor = Theme.currentTheme.linesColor
        self.view.backgroundColor = Theme.currentTheme.menuBackgroundColor
        editingMenuFaqLabel.textColor = Theme.currentTheme.textColor
        editingMenuFaqImageView.image = Settings.storage.currentThemeColor == 0 ? UIImage(named: "menu_info_light")! : UIImage(named: "menu_info_dark")!
        mainBackgroundView.backgroundColor = Theme.currentTheme.menuBackgroundColor
        
        editingMenuTableView.reloadData()
    }

    @objc func setText() {
        editingMenuFaqLabel.text = "BSOD_HELP".localized()
        editingMenu[0][0].name = "EDIT_WALLET".localized()
        editingMenu[0][1].name = "REFRESH".localized()
        editingMenu[0][2].name = "DELETE".localized()
        editingMenu[0][3].name = "OPEN_POOL".localized()
        editingMenu[1][0].name = "BSOD_POOL".localized()
        editingMenu[1][1].name = "TELEGRAM".localized()
        editingMenu[1][2].name = "DISCORD".localized()
        editingMenu[2][0].name = "NEWS".localized()
        editingMenu[3][0].name = "SETTINGS".localized()
        editingMenuTableView.reloadData()
    }

    private func fullUI() {
        editingMenuVersionLabel.text = "ver. \(ConfigHelper.Application.versionNumber)(\(ConfigHelper.Application.buildNumber))"
        editingMenuVersionLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }

    private func setupTableView() {
        editingMenuTableView.isScrollEnabled = false
        editingMenuTableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    private func openSettings() {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController")
        let navC = UINavigationController(rootViewController: vc)
        navC.isNavigationBarHidden = true
        self.slideMenuController()?.closeRight()
        self.slideMenuController()?.show(navC, sender: nil)
    }

    private func openSupportPage() {
        let storyboard = UIStoryboard(name: "SupportPage", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SupportPageViewController")
        let navC = UINavigationController(rootViewController: vc)
        navC.isNavigationBarHidden = true
        self.slideMenuController()?.closeRight()
        self.slideMenuController()?.show(navC, sender: nil)
    }

    private func openRemoveView() {
        let rootVC = self.slideMenuController()?.mainViewController as? WalletDetailViewController
        rootVC?.definesPresentationContext = true
        rootVC?.providesPresentationContextTransitionStyle = true
        rootVC?.overlayBlurredBackgroundView()
        
        let storyboard = UIStoryboard(name: "RemoveWallet", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "RemoveWalletViewController") as? RemoveWalletViewController else { return }
        vc.modalPresentationStyle = .overCurrentContext
        vc.wallet = wallet
        vc.delegate = rootVC
        self.slideMenuController()?.closeRight()
        present(vc, animated: true, completion: nil)
    }
    
    func removeWallet() {
        if let wallet = wallet {
            if wallet.status == .on {
                guard Network.isConnectedToNetwork() else {
                    showAlert(title: "ERROR".localized(), message: "NOTIFICATION_CONNECTION_ERROR".localized())
                    return
                }
                service.subscribeNotification(adress: wallet.adress ?? "", subscribe: 0) { [weak self] error in
                    if let error = error {
                        self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
                    } else {
                        Settings.storage.removeWallet(wallet: wallet)
                        NotificationCenter.default.post(name: notificationWalletRemoved, object: nil)
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            } else {
                Settings.storage.removeWallet(wallet: wallet)
                NotificationCenter.default.post(name: notificationWalletRemoved, object: nil)
                navigationController?.popViewController(animated: true)
            }
        }
    }

    private func openEditView() {
        let rootVC = self.slideMenuController()?.mainViewController as? WalletDetailViewController
        rootVC?.definesPresentationContext = true
        rootVC?.providesPresentationContextTransitionStyle = true
        rootVC?.overlayBlurredBackgroundView()
        
        let storyboard = UIStoryboard(name: "EditWallet", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "EditWalletViewController") as? EditWalletViewController else { return }
        vc.modalPresentationStyle = .overCurrentContext
        vc.wallet = wallet
        vc.delegate = rootVC
        self.slideMenuController()?.closeRight()
        present(vc, animated: true, completion: nil)
    }

}

extension EditingMenuViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "editCell", for: indexPath) as? EditingMenuTableViewCell else { return UITableViewCell() }

        cell.editingMenuNameLabel.text = editingMenu[indexPath.section][indexPath.row].name
        cell.editingMenuThumbnailImageView.image = Settings.storage.currentThemeColor == 0 ?
            editingMenu[indexPath.section][indexPath.row].imageLight :
            editingMenu[indexPath.section][indexPath.row].imageDark
        cell.editingMenuNameLabel.textColor = Theme.currentTheme.textColor
        cell.selectionStyle = .none

        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let lineView = UIView()
        let viewInView = UIView(frame: CGRect(x: lineView.frame.origin.x + 20, y: lineView.frame.origin.y, width: view.frame.width - 40, height: 1))
        viewInView.backgroundColor = Theme.currentTheme.linesColor
        lineView.addSubview(viewInView)

        return lineView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return editingMenu.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return editingMenu[section].count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let wallet = wallet else {
            showAlert(title: "ERROR".localized(), message: "WALLET_NOT_FOUND".localized())
            return
        }
        if indexPath == IndexPath(row: 0, section: 0) {
            self.openEditView()
        }
        else if indexPath == IndexPath(row: 1, section: 0) {
            NotificationCenter.default.post(name: WalletArrayRefreshNotification, object: nil)
            self.slideMenuController()?.closeRight()
        }
        else if indexPath == IndexPath(row: 2, section: 0) {
            if Settings.storage.confirmationDelete {
                openRemoveView()
            }
            else {
                removeWallet()
            }
        }
        else if indexPath == IndexPath(row: 3, section: 0), let adress = wallet.adress {
            URLOpenHelper().openURL("\(APIconstant.openBSODUrl)\(adress)")
        }
        else if indexPath == IndexPath(row: 0, section: 3) {
            self.openSettings()
        }
        else {
            self.editingMenu[indexPath.section][indexPath.row].action()
        }
    }
}
