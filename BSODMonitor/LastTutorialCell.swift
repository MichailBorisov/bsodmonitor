//
//  TutorialCell.swift
//  BSODMonitor
//
//  Created by Михаил on 16/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

class LastTutorialCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var closeButton: UIButton!

}
