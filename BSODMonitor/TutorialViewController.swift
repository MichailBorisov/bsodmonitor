//
//  TutorialViewController.swift
//  BSODMonitor
//
//  Created by Михаил on 16/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Localize_Swift

class TutorialViewController: UIViewController, UICollectionViewDelegateFlowLayout {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var images = TutorialImages().getImageArray()

    private lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.currentPage = 0
        pc.numberOfPages = images.count
        pc.currentPageIndicatorTintColor = ThemeColor.currentPageIndicatorTintColor
        pc.pageIndicatorTintColor = ThemeColor.pageIndicatorTintColor
        return pc
    }()
    
    private lazy var skipButton: UIButton = {
        let skipBtn = UIButton()
        skipBtn.setImage(#imageLiteral(resourceName: "skipButtonIcon"), for: .normal)
        skipBtn.addTarget(self, action: #selector(openNextVC), for: .touchUpInside)
        skipBtn.translatesAutoresizingMaskIntoConstraints = false
        return skipBtn
    }()
    
    private lazy var swipeArrowView: UIView = {
        let backgroundView = UIView()
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        let arrowImageView = UIImageView()
        arrowImageView.image = #imageLiteral(resourceName: "tutorialArrow")
        arrowImageView.translatesAutoresizingMaskIntoConstraints = false
        
        let swipeLabel = UILabel()
        swipeLabel.textAlignment = .center
        swipeLabel.text = "SWIPE_LEFT_TO_CONTINUE".localized()
        swipeLabel.font = UIFont(name: "Roboto", size: 14)
        swipeLabel.textColor = ThemeColor.defaultGray
        swipeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let skipLabel = UILabel()
        skipLabel.numberOfLines = 2
        skipLabel.textAlignment = .center
        skipLabel.text = "OR_TOUCH_TO_SKIP".localized()
        skipLabel.font = UIFont(name: "Roboto", size: 14)
        skipLabel.textColor = ThemeColor.defaultBlue
        skipLabel.translatesAutoresizingMaskIntoConstraints = false
        
        arrowImageView.addSubview(swipeLabel)
        NSLayoutConstraint.activate([
            swipeLabel.centerYAnchor.constraint(equalTo: arrowImageView.centerYAnchor),
            swipeLabel.leadingAnchor.constraint(equalTo: arrowImageView.leadingAnchor),
            swipeLabel.trailingAnchor.constraint(equalTo: arrowImageView.trailingAnchor)
            ])
        
        backgroundView.addSubview(skipLabel)
        NSLayoutConstraint.activate([
            skipLabel.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor, constant: 35),
            skipLabel.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor),
            skipLabel.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor)
            ])
        
        backgroundView.addSubview(arrowImageView)
        NSLayoutConstraint.activate([
            arrowImageView.topAnchor.constraint(equalTo: backgroundView.topAnchor),
            arrowImageView.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: -70),
            arrowImageView.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor),
            arrowImageView.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor)
            ])
        
        return backgroundView
    }()

    fileprivate func setupTopControl() {
        view.addSubview(pageControl)
        NSLayoutConstraint.activate([
            pageControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 55),
            pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pageControl.heightAnchor.constraint(equalToConstant: 10),
            pageControl.widthAnchor.constraint(equalToConstant: 90)
            ])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTopControl()
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeColors), name: ThemeChangeNotification, object: nil)
        scrollView.delegate = self
        exitButton.addTarget(self, action: #selector(openNextVC), for: .touchUpInside)
        
        backgroundView.backgroundColor = Theme.currentTheme.backgroundColor
    }

    @objc private func openNextVC() {
        if let viewController = self.navigationController?.viewControllers.first {
            if viewController is SupportPageViewController {
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            pushMainViewController()
        }
    }
    
    @objc private func changeColors() {
        images = TutorialImages().getImageArray()
        backgroundView.backgroundColor = Theme.currentTheme.backgroundColor
        scrollView.reloadInputViews()
    }

    private func pushMainViewController() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController")

        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let menuViewController = menuStoryboard.instantiateViewController(withIdentifier: "MainMenuViewController")

        let navC = UINavigationController(rootViewController: mainViewController)
        navC.setNavigationBarHidden(true, animated: true)

        let slideMenuController = SlideMenuController(mainViewController: navC, rightMenuViewController: menuViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true

        present(slideMenuController, animated: true, completion: nil)
    }

    override func viewDidLayoutSubviews() {
        for i in 0..<images.count {
            let imageView = UIImageView()
            let x = self.view.frame.width * CGFloat(i)
            imageView.frame = CGRect(x: x, y: 0, width: UIScreen.main.bounds.width, height: scrollView.frame.height)
            imageView.contentMode = .scaleToFill
            imageView.isUserInteractionEnabled = true
            
            // TODO: - Fix me for all laguages
            
            switch Localize.currentLanguage() {
            case "ru":
                imageView.image = UIImage(named: images[i].en)
            case "en":
                imageView.image = UIImage(named: images[i].en)
            case "fr":
                imageView.image = UIImage(named: images[i].en)
            case "de":
                imageView.image = UIImage(named: images[i].en)
            default:
                imageView.image = UIImage(named: images[i].en)
            }
            
            scrollView.contentSize.width = UIScreen.main.bounds.width * CGFloat(5)
            scrollView.addSubview(imageView)
            
            if i == 0 {
                imageView.addSubview(skipButton)
                imageView.addConstraints([
                    skipButton.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -(imageView.frame.height * 0.278)),
                    skipButton.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
                    skipButton.widthAnchor.constraint(equalToConstant: 40),
                    skipButton.heightAnchor.constraint(equalToConstant: 40)
                    ])
                
                imageView.addSubview(swipeArrowView)
                NSLayoutConstraint.activate([
                    swipeArrowView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -(imageView.frame.height * 0.278 + 40)),
                    swipeArrowView.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
                    swipeArrowView.widthAnchor.constraint(equalToConstant: 292),
                    swipeArrowView.heightAnchor.constraint(equalToConstant: 140)
                    ])
            }

        }
    }

}

extension TutorialViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        
        pageControl.currentPage = Int(x / view.frame.width)
        
        pageControl.currentPage == 4 ? exitButton.isHidden = false : (exitButton.isHidden = true)
    }
}
