//
//  CollectionViewCell.swift
//  BSODMonitor
//
//  Created by Михаил on 21/02/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class WorkersCell: UICollectionViewCell {
    
    var workers = [Miners]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionview.register(FreelancerCell.self, forCellWithReuseIdentifier: FreelancerCell.ide)
        collectionview.showsVerticalScrollIndicator = false
        collectionview.backgroundColor = UIColor.white
        return collectionView
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension WorkersCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        <#code#>
    }
}
