//
//  HomeTableHeaderView.swift
//  BSODMonitor
//
//  Created by Владислав on 01/03/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class HomeTableHeaderView: UITableViewHeaderFooterView {
    
    //Views
    @IBOutlet weak var firstHeaderView: UIView!
    @IBOutlet weak var firstFooterView: UIView!
    @IBOutlet weak var secondHeaderView: UIView!
    @IBOutlet weak var secondFooterView: UIView!
    @IBOutlet weak var thirdHeaderView: UIView!
    @IBOutlet weak var thirdFooterView: UIView!
    @IBOutlet weak var fourthHeaderView: UIView!
    @IBOutlet weak var fourthFooterView: UIView!
    @IBOutlet weak var fifthHeaderView: UIView!
    @IBOutlet weak var fifthFooterView: UIView!
    @IBOutlet weak var sixthHeaderView: UIView!
    @IBOutlet weak var sixthFooterView: UIView!
    @IBOutlet weak var seventhHeaderView: UIView!
    @IBOutlet weak var seventhFooterView: UIView!
    
    //Name Labels
    @IBOutlet weak var immatureNameLabel: UILabel!
    @IBOutlet weak var confirmedNameLabel: UILabel!
    @IBOutlet weak var valueNameLabel: UILabel!
    @IBOutlet weak var balanceNameLabel: UILabel!
    @IBOutlet weak var unpaidNameLabel: UILabel!
    @IBOutlet weak var paidNameLabel: UILabel!
    @IBOutlet weak var earnedNameLabel: UILabel!
    @IBOutlet weak var lastPayoutsNameLabel: UILabel!
    
    //Data Labels
    @IBOutlet weak var immatureLabel: UILabel!
    @IBOutlet weak var confirmedLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var unpaidLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var earnedLabel: UILabel!
    @IBOutlet weak var lastPayoutsLabel: UILabel!
    
    
    func setColors() {
        backgroundColor = .clear
        firstHeaderView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        secondHeaderView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        thirdHeaderView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        fourthHeaderView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        fifthHeaderView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        sixthHeaderView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        seventhHeaderView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        firstFooterView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        secondFooterView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        thirdFooterView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        fourthFooterView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        fifthFooterView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        sixthFooterView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        seventhFooterView.backgroundColor = Theme.currentTheme.workersFooterBackgroundViewColor
        
        immatureNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        confirmedNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        valueNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        balanceNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        unpaidNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        paidNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        earnedNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        lastPayoutsNameLabel.textColor = Theme.currentTheme.workersNameLabelColor
        
        immatureLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        confirmedLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        valueLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        balanceLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        unpaidLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        paidLabel.textColor = Theme.currentTheme.mainHeaderTextColor
        earnedLabel.textColor = Theme.currentTheme.mainHeaderTextColor
    }
    
    func setText() {
        immatureNameLabel.text = "IMMATURE".localized()
        confirmedNameLabel.text = "CONFIRMED".localized()
        valueNameLabel.text = "VALUE".localized()
        balanceNameLabel.text = "BALANCE".localized()
        unpaidNameLabel.text = "UNPAID".localized()
        paidNameLabel.text = "PAID".localized()
        earnedNameLabel.text = "EARNED".localized()
        lastPayoutsNameLabel.text = "LAST_PAYOUTS".localized() + ":"
        
        lastPayoutsNameLabel.font = UIFont(name: "Roboto", size: 15)
        lastPayoutsLabel.font = UIFont(name: "Roboto", size: 15)
    }
    
    func setData(entity: WalletModel) {
        immatureLabel.text = NSString(format: "%.4f", entity.walletInfo?.immature ?? 0) as String
        confirmedLabel.text = NSString(format: "%.4f", entity.walletInfo?.confirmed ?? 0) as String
        valueLabel.text = NSString(format: "%.4f", entity.walletInfo?.unsold ?? 0) as String
        balanceLabel.text = NSString(format: "%.4f", entity.walletInfo?.balance ?? 0) as String
        unpaidLabel.text = NSString(format: "%.4f", entity.walletInfo?.unpaid ?? 0) as String
        paidLabel.text = NSString(format: "%.4f", entity.walletInfo?.paid ?? 0) as String
        earnedLabel.text = NSString(format: "%.4f", entity.walletInfo?.total ?? 0) as String
        lastPayoutsLabel.text = NSString(format: "%.4f", entity.walletInfo?.paid24h ?? 0) as String
    }
}
