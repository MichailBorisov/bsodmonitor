//
//  AddWalletViewController.swift
//  BSODMonitor
//
//  Created by Михаил on 31/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

protocol AddWalletViewControllerDelegate: class {
    func removeBlurredBackgroundView(added: Bool)
}

protocol QRScannerDelegate: class {
    func didScan(walletName: String)
}

class AddWalletViewController: UIViewController {

    @IBOutlet weak var errorIcon: UIImageView!
    @IBOutlet weak var QRbutton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var adressTextView: UITextView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var secondBackgroundView: UIView!
    @IBOutlet weak var nameTextView: UITextView!
    @IBOutlet weak var walletParametersLabel: UILabel!
    @IBOutlet weak var addNewWalletNameLabel: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var loadingView: UIView!
    
    private let service = SearchRequestBuilder()

    weak var delegate: AddWalletViewControllerDelegate?

    var adressPlaceholderLabel = UILabel()
    var namePlaceholderLabel = UILabel()

    @IBAction func closeAction(_ sender: UITapGestureRecognizer) {
        dismissViewController(added: false)
    }

    @IBAction func qrCodeAction(_ sender: UIButton) {
        checkCameraAccess()
    }

    @IBAction func addWalletAction(_ sender: UIButton) {
        if adressTextView.text.matches("^[a-zA-Z0-9]{24,64}$") {
            if Settings.storage.getWalletsArray().contains(where: { $0.adress == adressTextView.text }) {
                showAlert(title: "ERROR".localized(), message: "WALLET_ALREADY_ADDED".localized())
            }
            else {
                makeRequest(walletAdress: adressTextView.text, walletName: nameTextView.text.isEmpty ? nil : nameTextView.text)
            }
        }
        else {
            showAlert(title: "WRONG_WALLET_NUMBER".localized(), message: "TRY_AGAIN".localized())
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addNewWalletNameLabel.text = "ADDNEWWALLET".localized()
        walletParametersLabel.text = "WALLETPARAMETERS".localized()
        adressTextView.delegate = self
        nameTextView.delegate = self
        errorIcon.isHidden = true
        addButton.setImage(UIImage(named: "Icon Accept New Wallet"), for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange), name: UIResponder.keyboardWillHideNotification, object: nil)

        changeColors()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupPlaceholders()
    }

    @objc func keyboardChange(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.view.frame.origin = .zero
        } else {
            self.view.frame.origin.y = -keyboardViewEndFrame.height
        }
    }

    func changeColors() {
        adressTextView.keyboardAppearance = Theme.currentTheme.keyboardTheme
        nameTextView.keyboardAppearance = Theme.currentTheme.keyboardTheme
        backgroundView.backgroundColor = Theme.currentTheme.addWalletBackgroundColor
        secondBackgroundView.backgroundColor = Theme.currentTheme.addWalletBackgroundColor
        nameView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        adressTextView.backgroundColor = Theme.currentTheme.backgroundColor
        nameTextView.backgroundColor = Theme.currentTheme.backgroundColor
        addButton.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        walletParametersLabel.textColor = Theme.currentTheme.filterTextColor
        adressTextView.textColor = Theme.currentTheme.textColor
        nameTextView.textColor = Theme.currentTheme.textColor
        addNewWalletNameLabel.textColor = Theme.currentTheme.walletsNameTextColor
    }

    func setupPlaceholders() {
        adressPlaceholderLabel.text = "ADRESS_PLACEHOLDER".localized()
        adressPlaceholderLabel.textColor = Theme.currentTheme.placeHolderTextColor
        adressPlaceholderLabel.font = UIFont(name: "Roboto", size: 16)
        adressPlaceholderLabel.sizeToFit()
        adressTextView.addSubview(adressPlaceholderLabel)
        adressPlaceholderLabel.frame.origin = CGPoint(x: adressTextView.frame.width / 2 - adressPlaceholderLabel.frame.width / 2, y: adressTextView.frame.height / 2 - adressPlaceholderLabel.frame.height / 2)
        adressPlaceholderLabel.isHidden = !adressTextView.text.isEmpty

        namePlaceholderLabel.text = "NAME_PLACEHOLDER".localized()
        namePlaceholderLabel.textColor = Theme.currentTheme.placeHolderTextColor
        namePlaceholderLabel.font = UIFont(name: "Roboto", size: 16)
        namePlaceholderLabel.sizeToFit()
        nameTextView.addSubview(namePlaceholderLabel)
        namePlaceholderLabel.frame.origin = CGPoint(x: nameTextView.frame.width / 2 - namePlaceholderLabel.frame.width / 2, y: nameTextView.frame.height / 2 - namePlaceholderLabel.frame.height / 2)
        namePlaceholderLabel.isHidden = !nameTextView.text.isEmpty
    }

    func makeRequest(walletAdress: String, walletName: String? = nil) {
        loadingView.isHidden = false
        service.search(walletName: walletAdress) { [weak self] result in
            self?.view.resignFirstResponder()
            switch result {
            case .success(let wallet):
                Settings.storage.addNewWallet(wallet: WalletModel(status: .off, adress: walletAdress, walletName: walletName, walletInfo: wallet, customSortIndex: Settings.storage.getWalletsArray().count, isExpanded: false))
                self?.dismissViewController(added: true)
            case .error(let error):
                self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
                self?.loadingView.isHidden = true
            }
        }
    }

    func dismissViewController(added: Bool) {
        view.endEditing(true)

        delegate?.removeBlurredBackgroundView(added: added)
        dismiss(animated: true, completion: nil)
    }

    func openScannerVC() {
        let vc = QRScannerViewController()
        vc.delegate = self

        show(vc, sender: nil)
    }

    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            presentCameraSettings()
        case .restricted:
            presentCameraSettings()
        case .authorized:
            openScannerVC()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    DispatchQueue.main.async {
                        self.openScannerVC()
                    }
                }
                else {
                    print("Permission denied")
                }
            }
        default:
            break
        }
    }

    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Error",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        })

        present(alertController, animated: true)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension AddWalletViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView == adressTextView {
            adressPlaceholderLabel.isHidden = !textView.text.isEmpty
            addButton.isEnabled = !adressTextView.text.isEmpty
        }
        else {
            namePlaceholderLabel.isHidden = !textView.text.isEmpty
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            if textView == adressTextView {
                nameTextView.becomeFirstResponder()
            }
            else {
                textView.resignFirstResponder()
            }
            return false
        }
        return true
    }
}

extension AddWalletViewController: QRScannerDelegate {
    func didScan(walletName: String) {
        adressTextView.text = ""
        adressTextView.text = walletName
        addButton.isEnabled = true
    }
}
