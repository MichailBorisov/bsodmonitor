//
//  MainViewController.swift
//  BSODMonitor
//
//  Created by Михаил on 31/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Localize_Swift
import AudioToolbox

let ThemeChangeNotification = Notification.Name("ThemeChangeNotification")
let LCLLanguageChangeNotification = Notification.Name("LCLLanguageChangeNotification")
let BatChangeNotification = Notification.Name("BatChangeNotification")
let WalletArrayRefreshNotification = Notification.Name("WalletArrayRefreshNotification")


class MainViewController: UIViewController {

    let service = SearchRequestBuilder()
    var refreshControl = UIRefreshControl()
    var sortChoosed: IndexPath?
    
    var dragInitialIndexPath: IndexPath?
    var dragHeaderCellSnapshot: UIView?
    var dragFooterCellSnapshot: UIView?
    var cellIsExpanded: Bool?
    var locationInTableView: CGPoint?
    var locationInHeader: CGPoint?
    var locationInFooter: CGPoint?
   
    var walletArray = [WalletModel]()
    var filterArray: [FilterArrayModel] = [
        FilterArrayModel(ruLang: "монета", enLang: "coin", deLang: "münze", frLang: "monnaie"),
        FilterArrayModel(ruLang: "майнеры", enLang: "workers", deLang: "arbeiter", frLang: "travailleurs"),
        FilterArrayModel(ruLang: "алгоритм", enLang: "algorithm", deLang: "algo", frLang: "algo"),
        FilterArrayModel(ruLang: "вручную", enLang: "custom", deLang: "benutzerdefiniert", frLang: "habitude")
    ]

    @IBOutlet weak var mainFirstView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var nextPayoutTime: UILabel!
    @IBOutlet weak var batImg: UIImageView!
    @IBOutlet weak var nextPayoutFromNow: UILabel!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var nextPayoutLabel: UILabel!

    @IBOutlet weak var angryBatImg: UIImageView!
    @IBOutlet weak var mainThirdView: UIView!
    @IBOutlet weak var mainSecondView: UIView!

    @IBAction func batTapped(_ sender: UITapGestureRecognizer) {
        BatActionHelper().batActions()
        angryBatImg.isHidden = !angryBat
    }
    
    @IBAction func openMenuTapped(_ sender: UIButton) {
        self.slideMenuController()?.openRight()
    }

    @IBAction func openWalletVC(_ sender: UIButton) {
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true

        overlayBlurredBackgroundView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextPayoutLabel.text = "NEXT_PAYOUT".localized()
        angryBatImage()


        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = ThemeColor.defaultBlue
        tableView.addSubview(refreshControl)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(HeaderCell.nib, forCellReuseIdentifier: HeaderCell.identifier)
        tableView.register(FooterCell.nib, forCellReuseIdentifier: FooterCell.identifier)

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(FilterCell.nib, forCellWithReuseIdentifier: FilterCell.identifier)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(onLongPressGesture(sender:)))
        longPress.minimumPressDuration = 0.5 // optional
        tableView.addGestureRecognizer(longPress)
        
        loadWallets()
        changeColors()

        sortSetup()
        notifications()
        
    }
    
    @objc func angryBatImage() {
        angryBatImg.isHidden = !angryBat
    }
    
    
    @objc func changeColors() {
        self.view.backgroundColor = Theme.currentTheme.backgroundColor
        mainFirstView.backgroundColor = Theme.currentTheme.backgroundColor
        mainSecondView.backgroundColor = Theme.currentTheme.backgroundColor
        collectionView.backgroundColor = Theme.currentTheme.sortBackgroundColor
        headerImageView.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "header_dark")! : UIImage(named: "header_light")!
        nextPayoutLabel.textColor = Theme.currentTheme.nextPayoutTextColor
        nextPayoutFromNow.textColor = Theme.currentTheme.nextPayoutTextColor
        mainTitleLabel.textColor = Theme.currentTheme.headerTextLabelColor
        menuButton.setImage(Settings.storage.currentThemeColor == 1 ? UIImage(named: "Icon Menu Dark")! : UIImage(named: "Icon Menu Light")!, for: .normal)
        batImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_dark")! : UIImage(named: "bat_light")!
        sortButton.setImage(Theme.currentTheme.arrayImg, for: .normal)
        angryBatImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_angry_dark")! : UIImage(named: "bat_angry_light")!
        tableView.reloadData()
        collectionViewReload()
    }


    @objc func refresh() {
        guard Network.isConnectedToNetwork() else {
            refreshControl.endRefreshing()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.showAlert(title: "ERROR".localized(), message: "CONNECTION_ERROR".localized())
            }
            return
        }
        let walletsAdress = walletArray.compactMap { $0.adress }
        var newArray = [WalletModel]()
        let dispatchQueue = DispatchQueue(label: "any-label-name")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        let dispatchGroup = DispatchGroup()
        dispatchQueue.async {
            for (index, adress) in walletsAdress.enumerated() {
                dispatchGroup.enter()
                self.service.search(walletName: adress) { [weak self] result in
                    switch result {
                    case .success(let wallet):
                        Settings.storage.updateWallet(wallet: wallet, walletAdress: adress)
                        let walletModel = WalletModel(status: self?.walletArray[index].status, adress: adress, walletName: self?.walletArray[index].walletName, walletInfo: wallet, customSortIndex: index, isExpanded: self?.walletArray[index].isExpanded ?? false)
                        newArray.append(walletModel)
                        dispatchSemaphore.signal()
                        dispatchGroup.leave()
                    case .error:
                        dispatchSemaphore.signal()
                        dispatchGroup.leave()
                    }
                }
                dispatchSemaphore.wait()
            }
            dispatchGroup.notify(queue: .main, execute: {
                DispatchQueue.main.async {
                    self.walletArray = newArray
                    self.refreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
            })
        }
        
        getNextPayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getNextPayout()
    }

    @objc func loadWallets() {
        walletArray = []
        let wallets = Settings.storage.getWalletsArray()

        for wallet in wallets {
            walletArray.append(wallet)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @objc func onLongPressGesture(sender: UILongPressGestureRecognizer) {
        let locationInView = sender.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: locationInView)
        
        if sender.state == .began {
            if indexPath != nil {
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                dragInitialIndexPath = indexPath
                cellIsExpanded = walletArray[dragInitialIndexPath?.section ?? 0].isExpanded
                guard let headerCell = tableView.cellForRow(at: IndexPath(row: 0, section: indexPath!.section)) else { return }
                guard let footerCell = tableView.cellForRow(at: IndexPath(row: 1, section: indexPath!.section)) else { return }
                dragHeaderCellSnapshot = snapshotOfCell(inputView: headerCell)
                dragFooterCellSnapshot = snapshotOfCell(inputView: footerCell)
                var headerCenter = headerCell.center
                var footerCenter = footerCell.center
                let headerDragRate = (self.locationInHeader?.y ?? CGFloat()) - (self.locationInTableView?.y ?? CGFloat())
                let footerDragRate = (self.locationInFooter?.y ?? CGFloat()) - (self.locationInTableView?.y ?? CGFloat())
                let footerRate = (self.dragHeaderCellSnapshot?.bounds.height ?? CGFloat()) * 0.5 + (self.dragFooterCellSnapshot?.bounds.height ?? CGFloat()) * 0.5
                locationInHeader = headerCenter
                locationInFooter = footerCenter
                if self.cellIsExpanded! {
                    footerCenter.y = locationInView.y + footerDragRate
                    headerCenter.y = footerCenter.y - footerRate
                    self.dragHeaderCellSnapshot?.center = headerCenter
                    self.dragFooterCellSnapshot?.center = footerCenter
                } else {
                    headerCenter.y = locationInView.y + headerDragRate
                    self.dragHeaderCellSnapshot?.center = headerCenter
                }
                dragHeaderCellSnapshot?.alpha = 0.0
                dragFooterCellSnapshot?.alpha = 0.0
                tableView.addSubview(dragHeaderCellSnapshot ?? UIView())
                tableView.addSubview(dragFooterCellSnapshot ?? UIView())

                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.dragHeaderCellSnapshot?.transform = (self.dragHeaderCellSnapshot?.transform.scaledBy(x: 1.05, y: 1.05))!
                    self.dragHeaderCellSnapshot?.alpha = 0.99
                    headerCell.alpha = 0.0
                    
                    self.dragFooterCellSnapshot?.transform = (self.dragFooterCellSnapshot?.transform.scaledBy(x: 1.05, y: 1.05) ?? CGAffineTransform())
                    self.dragFooterCellSnapshot?.alpha = 0.99
                    footerCell.alpha = 0.0
                }, completion: { (finished) -> Void in
                    if finished {
                        headerCell.isHidden = true
                        footerCell.isHidden = true
                    }
                })
            }
            locationInTableView = locationInView
            Settings.storage.sortChoosed = 3
            collectionViewReload()
        } else if sender.state == .changed && dragInitialIndexPath != nil {
            guard var headerCenter = dragHeaderCellSnapshot?.center  else { return }
            guard var footerCenter = dragFooterCellSnapshot?.center else { return }
            let headerDragRate = (locationInHeader?.y ?? CGFloat()) - (locationInTableView?.y ?? CGFloat())
            let footerDragRate = (locationInFooter?.y ?? CGFloat()) - (locationInTableView?.y ?? CGFloat())
            let footerRate = (self.dragHeaderCellSnapshot?.bounds.height ?? CGFloat()) * 0.525 + (self.dragFooterCellSnapshot?.bounds.height ?? CGFloat()) * 0.525
            

            if cellIsExpanded! {
                footerCenter.y = locationInView.y + footerDragRate
                headerCenter.y = footerCenter.y - footerRate
                dragHeaderCellSnapshot?.center = headerCenter
                dragFooterCellSnapshot?.center = footerCenter
            } else {
                headerCenter.y = locationInView.y + headerDragRate
                dragHeaderCellSnapshot?.center = headerCenter
            }
            
            if indexPath != nil && indexPath != dragInitialIndexPath {
                
                let dataToMove = walletArray[dragInitialIndexPath!.section]
                walletArray.remove(at: dragInitialIndexPath!.section)
                walletArray.insert(dataToMove, at: indexPath!.section)

                tableView.moveSection(dragInitialIndexPath!.section, toSection: indexPath!.section)
                dragInitialIndexPath = indexPath
                
                for item in 0 ..< walletArray.count {
                    walletArray[item].customSortIndex = item
                    Settings.storage.updateWalletIndex(wallet: walletArray[item], newIndex: item)
                }
            }
        } else if sender.state == .ended && dragInitialIndexPath != nil {
            guard let headerCell = tableView.cellForRow(at: IndexPath(row: 0, section: dragInitialIndexPath?.section ?? 0)) else { return }
            guard let footerCell = tableView.cellForRow(at: IndexPath(row: 1, section: dragInitialIndexPath?.section ?? 0)) else { return }
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.dragHeaderCellSnapshot?.center = headerCell.center
                self.dragFooterCellSnapshot?.center = footerCell.center
                self.dragHeaderCellSnapshot?.transform = CGAffineTransform.identity
                self.dragHeaderCellSnapshot?.alpha = 0.0
                self.dragFooterCellSnapshot?.transform = CGAffineTransform.identity
                self.dragFooterCellSnapshot?.alpha = 0.0
                headerCell.isHidden = false
                footerCell.isHidden = false
                headerCell.alpha = 1.0
                footerCell.alpha = 1.0
            }, completion: { (finished) -> Void in
                if finished {
                    self.dragInitialIndexPath = nil
                    self.dragHeaderCellSnapshot?.removeFromSuperview()
                    self.dragFooterCellSnapshot?.removeFromSuperview()
                    
                    self.dragHeaderCellSnapshot = nil
                    self.dragFooterCellSnapshot = nil
                }
            })
            tableView.reloadData()
        }
    }
    
    func snapshotOfCell(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cellSnapshot = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    func sortSetup() {
        switch Settings.storage.sortChoosed {
        case 0:
            walletArray.sort(by: { $0.walletInfo?.currency ?? "" < $1.walletInfo?.currency ?? "" })
        case 1:
            walletArray.sort(by: { $0.walletInfo?.miners?.count ?? 0 < $1.walletInfo?.miners?.count ?? 0 })
        case 2:
            walletArray.sort(by: { $0.walletInfo?.miners?.first?.algo ?? "" < $1.walletInfo?.miners?.first?.algo ?? "" })
        case 3:
            walletArray.sort(by: { $0.customSortIndex < $1.customSortIndex })
        default:
            break
        }
        
        filterArray[Settings.storage.sortChoosed].isSelected = true
        collectionView.selectItem(at: IndexPath(row: 0, section: Settings.storage.sortChoosed), animated: true, scrollPosition: .left)
    }
    
    func notifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(loadWallets), name: notificationWalletPushUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadWallets), name: notificationWalletName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadWallets), name: notificationWalletRemoved, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeColors), name: ThemeChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(languageReload), name: LCLLanguageChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(angryBatImage), name: BatChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: WalletArrayRefreshNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: updateWalletMainScreen, object: nil)
    }
    
    @objc func languageReload() {
        collectionViewReload()
        nextPayoutLabel.text = "NEXT_PAYOUT".localized()
    }

    func openDetailScreen(wallet: WalletModel) {
        let mainStoryboard = UIStoryboard(name: "WalletDetail", bundle: nil)
        guard let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: "WalletDetailViewController") as? WalletDetailViewController else { return }
        mainViewController.wallet = wallet

        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        guard let menuViewController = menuStoryboard.instantiateViewController(withIdentifier: "EditingMenuViewController") as? EditingMenuViewController else { return }
        menuViewController.wallet = wallet

        let slideMenuController = SlideMenuController(mainViewController: mainViewController, rightMenuViewController: menuViewController)
        slideMenuController.changeRightViewWidth(UIScreen.main.bounds.width - 60)

        navigationController?.show(slideMenuController, sender: nil)
    }
    
    func collectionViewReload() {
        let collectionViewIndexPath = Settings.storage.sortChoosed
            collectionView.reloadData()
            collectionView.selectItem(at: IndexPath(row: 0, section: collectionViewIndexPath), animated: true, scrollPosition: .left)
    }
    
    @IBAction func reversButtonTappedAction(_ sender: UIButton) {
        walletArray.reverse()
        tableView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if identifier == "wallet" {
                if let viewController = segue.destination as? AddWalletViewController {
                    viewController.modalPresentationStyle = .overFullScreen
                    viewController.delegate = self
                }
            }
        }
    }

    private func getNextPayout() {
        service.getNextPayout { [weak self] result in
            switch result {
            case .success(let data):
                self?.setupNextPayout(data: data)
            case .error(let error):
                self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
            }
        }
    }

    private func setupNextPayout(data: NextPayout) {
        if let intDate = data.nextpayout {
            let date = Date(timeIntervalSince1970: Double(intDate))

            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            formatter.locale = .current

            let timeToDate = Date().offsetFrom(date: date)

            nextPayoutTime.text = formatter.string(from: date)
            nextPayoutFromNow.text = timeToDate
        }
    }
    
    private func openRemoveViewForWalletAt(indexPath: IndexPath) {
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        
        overlayBlurredBackgroundView()
        
        let storyboard = UIStoryboard(name: "RemoveWallet", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "RemoveWalletViewController") as? RemoveWalletViewController else { return }
        vc.modalPresentationStyle = .overFullScreen
        vc.wallet = walletArray[indexPath.section]
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func removeWalletAt(indexPath: IndexPath) {
        if let status = walletArray[indexPath.section].status, status == .on {
            guard Network.isConnectedToNetwork() else {
                refreshControl.endRefreshing()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.showAlert(title: "ERROR".localized(), message: "NOTIFICATION_CONNECTION_ERROR".localized())
                }
                return
            }
            service.subscribeNotification(adress: walletArray[indexPath.section].adress ?? "", subscribe: 0) { [weak self] error in
                if let error = error {
                    self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
                } else {
                    Settings.storage.removeWallet(wallet: (self?.walletArray[indexPath.section])!)
                    self?.loadWallets()
                }
            }
        } else {
            Settings.storage.removeWallet(wallet: walletArray[indexPath.section])
            loadWallets()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return walletArray.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCell.identifier) as? HeaderCell else { return UITableViewCell() }
            cell.wallenNameLabel.text = walletArray[indexPath.section].walletName != nil ? walletArray[indexPath.section].walletName : walletArray[indexPath.section].adress
            cell.openButton.setImage(walletArray[indexPath.section].isExpanded ? #imageLiteral(resourceName: "Icon Arrow Cell Close") : #imageLiteral(resourceName: "Icon Arrow Cell") , for: .normal)
            
            if let status = walletArray[indexPath.section].status {
                switch status {
                case .on:
                    cell.statusView.backgroundColor = .white
                    if walletArray[indexPath.section].walletInfo?.miners == nil || walletArray[indexPath.section].walletInfo?.miners?.isEmpty ?? true {
                        cell.statusView.backgroundColor = .red
                    }
                case .off:
                    cell.statusView.backgroundColor = .clear
                case .unknown:
                    cell.statusView.backgroundColor = .red
                }
            }
            
            cell.openDetailCell = { [weak self] in
                guard let weakSelf = self else { return }
                weakSelf.walletArray[indexPath.section].isExpanded = !weakSelf.walletArray[indexPath.section].isExpanded
                Settings.storage.updateWalletIsExpandedStatus(wallet: weakSelf.walletArray[indexPath.section], isExpanded: weakSelf.walletArray[indexPath.section].isExpanded)
                weakSelf.tableView.beginUpdates()
                weakSelf.tableView.reloadRows(at: [indexPath], with: .automatic)
                weakSelf.tableView.endUpdates()
                weakSelf.view.layoutIfNeeded()
            }

            cell.openDetailScreen = { [weak self] in
                guard let weakSelf = self else { return }
                weakSelf.openDetailScreen(wallet: weakSelf.walletArray[indexPath.section])
            }
            
            cell.openButton.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
            cell.openDetailScreenButton.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
            cell.wallenNameLabel.textColor = Theme.currentTheme.mainHeaderTextColor
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FooterCell.identifier) as? FooterCell else { return UITableViewCell() }

            cell.setupView(wallet: walletArray[indexPath.section])
            cell.firstFooterView.backgroundColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            cell.secondFooterView.backgroundColor = Theme.currentTheme.footerBackgroundViewColor
            cell.hashrateNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
            cell.workersNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
            cell.unpaidNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
            cell.balanceNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
            cell.coinName.textColor = Theme.currentTheme.mainFooterTextColor
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if indexPath.row == 1 {
            let repeatAction = UITableViewRowAction(style: .default, title: "DELETE".localized()) { _,_ in
                
                if Settings.storage.confirmationDelete {
                    self.openRemoveViewForWalletAt(indexPath: indexPath)
                }
                else {
                    self.removeWalletAt(indexPath: indexPath)
                }
            }
            
            return [repeatAction]
        } else {
            return []
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 25
        } else {
            return walletArray[indexPath.section].isExpanded ? 75 : 0
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            openDetailScreen(wallet: walletArray[indexPath.section])
        }
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return filterArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterCell.identifier, for: indexPath) as? FilterCell else { return UICollectionViewCell() }

        switch Localize.currentLanguage() {
        case "ru": cell.nameLabel.text = filterArray[indexPath.section].ruLang
        case "en": cell.nameLabel.text = filterArray[indexPath.section].enLang
        case "fr": cell.nameLabel.text = filterArray[indexPath.section].frLang
        case "de": cell.nameLabel.text = filterArray[indexPath.section].deLang
        default: print("error")
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !filterArray[indexPath.section].isSelected {
            switch indexPath.section {
            case 0:
                walletArray.sort(by: { $0.walletInfo?.currency ?? "" < $1.walletInfo?.currency ?? "" })
            case 1:
                walletArray.sort(by: { $0.walletInfo?.miners?.count ?? 0 < $1.walletInfo?.miners?.count ?? 0 })
            case 2:
                walletArray.sort(by: { $0.walletInfo?.miners?.first?.algo ?? "" < $1.walletInfo?.miners?.first?.algo ?? "" })
            case 3:
                walletArray.sort(by: { $0.customSortIndex < $1.customSortIndex })
            default:
                break
            }
            filterArray[indexPath.section].isSelected = !filterArray[indexPath.section].isSelected
            Settings.storage.sortChoosed = indexPath.section
            tableView.reloadData()
        } else {
            walletArray.reverse()
            tableView.reloadData()
        }
    }

}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if Localize.currentLanguage() == "de" {
            if indexPath == IndexPath(row: 0, section: 3) {
                return CGSize(width: (self.view.bounds.width - 55) / 2.8, height: self.collectionView.bounds.height)
            } else {
                return CGSize(width: (self.view.bounds.width - 55) / 4.66, height: self.collectionView.bounds.height)
            }
        } else {
            return CGSize(width: (self.view.bounds.width - 55) / 4, height: self.collectionView.bounds.height)
        }
    }
}

extension MainViewController: AddWalletViewControllerDelegate {
    func removeBlurredBackgroundView(added: Bool) {
        let subviews = view.subviews.filter { $0.isKind(of: UIVisualEffectView.self) }
        subviews.first?.removeFromSuperview()
        added ? loadWallets() : ()
    }
}

extension MainViewController: WalletDetailDelegate {
    func closeDetailScreen() {
        let subviews = view.subviews.filter { $0.isKind(of: UIVisualEffectView.self) }
        subviews.first?.removeFromSuperview()
    }
    
    func removeBlurredBackgroundView() {
        let subviews = view.subviews.filter { $0.isKind(of: UIVisualEffectView.self) }
        subviews.first?.removeFromSuperview()
    }
}
