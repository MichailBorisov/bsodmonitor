//
//  LanguageViewController.swift
//  BSODMonitor
//
//  Created by Владислав on 29/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit
import Localize_Swift

class LanguageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerImg: UIImageView!
    @IBOutlet weak var angryBatImg: UIImageView!
    @IBOutlet weak var batImg: UIImageView!
    @IBOutlet weak var iconLangImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var topLine: UIView!
    
    struct LanguagesArray {
        var name: String
        var enName: String
        var localizeName: String
    }

    let languagesArray: [LanguagesArray] = [
        LanguagesArray(name: "Deutsch", enName: "German", localizeName: "de"),
        LanguagesArray(name: "English", enName: "English", localizeName: "en"),
        LanguagesArray(name: "Français", enName: "French", localizeName: "fr"),
        LanguagesArray(name: "Русский", enName: "Russian", localizeName: "ru")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isScrollEnabled = false
        changeColors()
        nameLabel.text = "LANGUAGE".localized()
        
        angryBatImage()
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeColors), name: ThemeChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(angryBatImage), name: BatChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func batTapped(_ sender: UITapGestureRecognizer) {
        BatActionHelper().batActions()
        angryBatImg.isHidden = !angryBat
    }

    @IBAction func backAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func angryBatImage() {
        angryBatImg.isHidden = !angryBat
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return languagesArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "languageCell")
        cell.textLabel?.text = languagesArray[indexPath.section].name
        cell.detailTextLabel?.text = languagesArray[indexPath.section].enName
        cell.textLabel?.textColor = Theme.currentTheme.textColor
        cell.detailTextLabel?.textColor = Theme.currentTheme.textColor
        cell.backgroundColor = Theme.currentTheme.backgroundColor
        cell.selectionStyle = .none

        if languagesArray[indexPath.section].localizeName == Localize.currentLanguage() {
            cell.accessoryType = .checkmark
        }
        return cell
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let lineView = UIView()
        let viewInView = UIView(frame: CGRect(x: lineView.frame.origin.x + 15, y: lineView.frame.origin.y, width: view.frame.width - 30, height: 1))
        viewInView.backgroundColor = Theme.currentTheme.linesColor
        lineView.addSubview(viewInView)
        return lineView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    @objc func changeColors() {
       self.view.backgroundColor = Theme.currentTheme.backgroundColor
        headerImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "header_dark")! : UIImage(named: "header_light")!
        batImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_dark")! : UIImage(named: "bat_light")!
        iconLangImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "Icon Language Dark")! : UIImage(named: "Icon Language Light")!
        nameLabel.textColor = Theme.currentTheme.headerTextLabelColor
        tableView.backgroundColor = Theme.currentTheme.backgroundColor
        angryBatImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_angry_dark")! : UIImage(named: "bat_angry_light")!
        
        tableView.reloadData()

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark

        switch indexPath.section {
        case 0: Localize.setCurrentLanguage("de")
        case 1: Localize.setCurrentLanguage("en")
        case 2: Localize.setCurrentLanguage("fr")
        case 3: Localize.setCurrentLanguage("ru")
        default: print("error")
        }

        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }
}
