//
//  HomeTableViewCell.swift
//  BSODMonitor
//
//  Created by Владислав on 02/03/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit
import Localize_Swift

class HomeTableViewCell: UITableViewCell {
    
    var timeString = ""

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var payoutTimeLabel: UILabel!
    @IBOutlet weak var payoutAmountLabel: UILabel!
    
    func setup(indexPath: Int, payout: [Payout]) {
        if indexPath == 0 {
            backgroundColor = Theme.currentTheme.backgroundWalletStringColor
            lineView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
            payoutTimeLabel.text = "TIME".localized()
            payoutAmountLabel.text = "AMOUNT".localized()
        } else if indexPath % 2 == 0 {
            backgroundColor = Theme.currentTheme.evenTableViewCellsBackground
            lineView.backgroundColor = Theme.currentTheme.unevenTableViewCellsBackground
            payoutTimeLabel.text = timeTextLocalization(time: payout[indexPath - 1].time ?? "")
            payoutAmountLabel.text = payout[indexPath - 1].amount
            payoutTimeLabel.textColor = Theme.currentTheme.homeCellTextColor
            payoutAmountLabel.textColor = Theme.currentTheme.homeCellTextColor
        } else {
            backgroundColor = Theme.currentTheme.unevenTableViewCellsBackground
            lineView.backgroundColor = Theme.currentTheme.evenTableViewCellsBackground
            payoutTimeLabel.text = timeTextLocalization(time: payout[indexPath - 1].time ?? "")
            payoutAmountLabel.text = payout[indexPath - 1].amount
            payoutTimeLabel.textColor = Theme.currentTheme.homeCellTextColor
            payoutAmountLabel.textColor = Theme.currentTheme.homeCellTextColor
        }
    }
    
    func timeTextLocalization(time: String) -> String{
        let numbersString = time.filter { "0123456789".contains($0) }
        let numbersSuffix = Int(numbersString.suffix(2))
        let oneNumberSuffix = Int(numbersString.suffix(1))
        if time.contains("hours") {
            if Localize.currentLanguage() == "fr" {
                timeString = time.replacingOccurrences(of: "hours", with: "heures", options: NSString.CompareOptions.literal, range:nil)
                let localizedText = "il y a " + timeString
                return localizedText
            } else if Localize.currentLanguage() == "de" {
                timeString = time.replacingOccurrences(of: "hours", with: "studen", options: NSString.CompareOptions.literal, range:nil)
                let localizedText = "vor " + timeString
                return localizedText
            } else if Localize.currentLanguage() == "ru" {
                if (numbersSuffix ?? 0 < 10) || (numbersSuffix ?? 0 > 20) {
                    if oneNumberSuffix == 1 {
                        timeString = time.replacingOccurrences(of: "hours", with: "час назад", options: NSString.CompareOptions.literal, range:nil)
                    } else if (oneNumberSuffix == 2) || (oneNumberSuffix == 3) || (oneNumberSuffix == 4) {
                        timeString = time.replacingOccurrences(of: "hours", with: "часа назад", options: NSString.CompareOptions.literal, range:nil)
                    } else {
                        timeString = time.replacingOccurrences(of: "hours", with: "часов назад", options: NSString.CompareOptions.literal, range:nil)
                    }
                } else {
                    timeString = time.replacingOccurrences(of: "hours", with: "часов назад", options: NSString.CompareOptions.literal, range:nil)
                }
                return timeString
            } else {
                if numbersSuffix == 1 {
                    timeString = time.replacingOccurrences(of: "hours", with: "hour ago", options: NSString.CompareOptions.literal, range:nil)
                } else {
                    timeString = time.replacingOccurrences(of: "hours", with: "hours ago", options: NSString.CompareOptions.literal, range:nil)
                }
                return timeString
            }
        } else {
            if Localize.currentLanguage() == "fr" {
                timeString = time.replacingOccurrences(of: "mins", with: "minutes", options: NSString.CompareOptions.literal, range:nil)
                let localizedText = "il y a " + timeString
                return localizedText
            } else if Localize.currentLanguage() == "de" {
                timeString = time.replacingOccurrences(of: "mins", with: "minuten", options: NSString.CompareOptions.literal, range:nil)
                let localizedText = "vor " + timeString
                return localizedText
            }  else if Localize.currentLanguage() == "ru" {
                if (numbersSuffix ?? 0 < 10) || (numbersSuffix ?? 0 > 20) {
                    if oneNumberSuffix == 1 {
                        timeString = time.replacingOccurrences(of: "mins", with: "минута назад", options: NSString.CompareOptions.literal, range:nil)
                    } else if (oneNumberSuffix == 2) || (oneNumberSuffix == 3) || (oneNumberSuffix == 4) {
                        timeString = time.replacingOccurrences(of: "mins", with: "минуты назад", options: NSString.CompareOptions.literal, range:nil)
                    } else {
                        timeString = time.replacingOccurrences(of: "mins", with: "минут назад", options: NSString.CompareOptions.literal, range:nil)
                    }
                } else {
                    timeString = time.replacingOccurrences(of: "mins", with: "минут назад", options: NSString.CompareOptions.literal, range:nil)
                }
                return timeString
            } else {
                if numbersSuffix == 1 {
                    timeString = time.replacingOccurrences(of: "mins", with: "minute ago", options: NSString.CompareOptions.literal, range:nil)
                } else {
                    timeString = time.replacingOccurrences(of: "mins", with: "minutes ago", options: NSString.CompareOptions.literal, range:nil)
                }
                return timeString
            }
        }
    }
}
