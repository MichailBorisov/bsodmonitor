//
//  BatActionHelper.swift
//  BSODMonitor
//
//  Created by Владислав on 18/02/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

var timesBatTapped: Int = 0
var counter = 0
var timer = Timer()
var angryBat: Bool = false
var themeChangerTaps = 0

final class BatActionHelper {
    func batActions() {
        switch timesBatTapped {
        case 0:
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            timesBatTapped += 1
        case 1..<9:
            timesBatTapped += 1
        case 9:
            timesBatTapped += 1
            angryBat = true
            NotificationCenter.default.post(name: BatChangeNotification, object: nil)
        case 10..<29:
            timesBatTapped += 1
        case 29:
            timesBatTapped = 0
            URLOpenHelper().openURL("https://bsod.pw/egg")
        default: print("Unknown error")
        }
        themeChangerTaps += 1
    }
    
    @objc func timerAction() {
        themeChanger()
        themeChangerTaps = 0
        if counter < 60 {
            counter += 1
        } else {
            timer.invalidate()
            counter = 0
            timesBatTapped = 0
        }
    }
    
    func themeChanger() {
        if themeChangerTaps == 1 {
            if Settings.storage.currentThemeColor == 1 {
                Theme.currentTheme = LightTheme()
                Settings.storage.currentThemeColor = 0
            } else {
                Theme.currentTheme = DarkTheme()
                Settings.storage.currentThemeColor = 1
            }
            NotificationCenter.default.post(name: ThemeChangeNotification, object: nil)
        }
    }
}
