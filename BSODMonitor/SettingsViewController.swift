//
//  SettingsViewController.swift
//  BSODMonitor
//
//  Created by Михаил on 01/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import Localize_Swift

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var nameSettings: UILabel!
    @IBOutlet weak var themeLabel: UILabel!
    @IBOutlet weak var lightLabel: UILabel!
    @IBOutlet weak var darkLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var deleteConf: UILabel!
    @IBOutlet weak var languagezSelector: UIButton!
    @IBOutlet weak var themeView: UIView!
    @IBOutlet weak var confirmationView: UIView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var line1View: UIView!
    @IBOutlet weak var themeImg: UIImageView!
    @IBOutlet weak var headerBackgroundImage: UIImageView!
    @IBOutlet weak var batImage: UIImageView!
    @IBOutlet weak var angryBatImg: UIImageView!
    @IBOutlet weak var headerSettingsImg: UIImageView!
    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var langImg: UIImageView!
    @IBOutlet weak var deleteImg: UIImageView!
    @IBOutlet weak var deleteConfirmationSwitch: UISwitch!
    @IBOutlet weak var colorSwitch: UISwitch!
    @IBOutlet weak var line2View: UIView!

    @IBAction func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func batTapped(_ sender: UITapGestureRecognizer) {
        BatActionHelper().batActions()
        angryBatImg.isHidden = !angryBat
    }
    
    @IBAction func deleteConfiramtionSwitchAction(_ sender: UISwitch) {
        Settings.storage.confirmationDelete = sender.isOn
    }

    @IBAction func themeSwitch(_ sender: UISwitch) {
        Theme.currentTheme = sender.isOn ? DarkTheme() : LightTheme()
        Settings.storage.currentThemeColor = sender.isOn ? 1 : 0
        NotificationCenter.default.post(name: ThemeChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        colorSwitch.isOn = Settings.storage.currentThemeColor == 1 ? true : false
        deleteConfirmationSwitch.isOn = Settings.storage.confirmationDelete ? true : false

        setText()
        changeTheme()
        angryBatImage()

        switch Localize.currentLanguage() {
        case "ru":
            languagezSelector.setTitle("Русский", for: UIControl.State.normal)
        case "en":
            languagezSelector.setTitle("English", for: UIControl.State.normal)
        case "fr":
            languagezSelector.setTitle("Français", for: UIControl.State.normal)
        case "de":
            languagezSelector.setTitle("Deutsch", for: UIControl.State.normal)
        default: break
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeTheme), name: ThemeChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: LCLLanguageChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(angryBatImage), name: BatChangeNotification, object: nil)
    }
    
    @objc func angryBatImage() {
        angryBatImg.isHidden = !angryBat
    }

    @objc func setText() {
        nameSettings.text = "SETTINGS".localized()
        themeLabel.text = "THEME".localized()
        lightLabel.text = "LIGHT".localized()
        darkLabel.text = "DARK".localized()
        languageLabel.text = "LANGUAGE".localized()
        deleteConf.text = "DELETECONF".localized()
        languagezSelector.setTitle("CHOOSED_LANGUAGE".localized(), for: UIControl.State.normal)

    }

    @IBAction func languageSelector(_ sender: UIButton) {
        let languageStoryboard = UIStoryboard(name: "Language", bundle: nil)
        let languageController = languageStoryboard.instantiateViewController(withIdentifier: "LanguageViewController")

        navigationController?.pushViewController(languageController, animated: true)
    }

    @objc func changeTheme() {
        self.view.backgroundColor = Theme.currentTheme.backgroundColor
        themeView.backgroundColor = Theme.currentTheme.backgroundColor
        confirmationView.backgroundColor = Theme.currentTheme.backgroundColor
        languageView.backgroundColor = Theme.currentTheme.backgroundColor
        themeLabel.textColor = Theme.currentTheme.textColor
        darkLabel.textColor = Theme.currentTheme.textColor
        lightLabel.textColor = Theme.currentTheme.textColor
        colorSwitch.isOn ? (darkLabel.textColor = ThemeColor.defaultBlue) : (lightLabel.textColor = ThemeColor.defaultBlue)
        deleteConf.textColor = Theme.currentTheme.textColor
        languageLabel.textColor = Theme.currentTheme.textColor
        line1View.backgroundColor = Theme.currentTheme.linesColor
        line2View.backgroundColor = Theme.currentTheme.linesColor
        langImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "Icon_lang_dark")! : UIImage(named: "Icon_lang_light")!
        deleteImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "Icon_delete_dark")! : UIImage(named: "Icon_delete_light")!
        themeImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "Icon_theme_dark")! : UIImage(named: "Icon_theme_light")!
        headerBackgroundImage.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "header_dark")! : UIImage(named: "header_light")!
        batImage.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_dark")! : UIImage(named: "bat_light")!
        headerSettingsImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "Header_settings_dark")! : UIImage(named: "Header_settings_light")!
        angryBatImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_angry_dark")! : UIImage(named: "bat_angry_light")!
        nameSettings.textColor = Theme.currentTheme.headerTextLabelColor
    }
}
