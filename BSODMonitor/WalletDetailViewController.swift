//
//  WalletDetailViewController.swift
//  BSODMonitor
//
//  Created by Михаил on 06/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit
import UserNotifications

let notificationWalletName = Notification.Name("updateWalletName")
let notificationWalletRemoved = Notification.Name("walletRemoved")
let notificationWalletPushUpdate = Notification.Name("statusUpdated")
let updateWalletMainScreen = Notification.Name("updateWalletMainScreen")

protocol WalletDetailDelegate: class {
    func removeBlurredBackgroundView()
    func closeDetailScreen()
}

class WalletDetailViewController: UIViewController {

    var wallet: WalletModel?
    let service = SearchRequestBuilder()
    var selectedButton = 0

    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var angryBatImg: UIImageView!
    @IBOutlet weak var headerBackgroundImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var batImage: UIImageView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var mainBackgroundView: UIView!
    
    @IBOutlet weak var nextPayoutLabel: UILabel!
    @IBOutlet weak var nextPayoutTime: UILabel!
    @IBOutlet weak var nextPayoutFromNow: UILabel!
    
    //CollectionView
    @IBOutlet weak var infoCollectionView: UICollectionView!
    
    //Filter footer buttons
    @IBOutlet weak var homeButton: UIGestureRecognizer!
    @IBOutlet weak var workersButton: UIGestureRecognizer!
    @IBOutlet weak var earningsButton: UIGestureRecognizer!
    @IBOutlet weak var homeButtonImageView: UIImageView!
    @IBOutlet weak var workersButtonImageView: UIImageView!
    @IBOutlet weak var earningsButtonImageView: UIImageView!
    @IBOutlet weak var homeButtonLabel: UILabel!
    @IBOutlet weak var workersButtonLabel: UILabel!
    @IBOutlet weak var earningsButtonLabel: UILabel!
    
    //Header Wallet cell
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var walletName: UILabel!
    @IBOutlet weak var headerBackgroundView: UIView!
    
    //Footer Wallet cell
    @IBOutlet weak var firstFooterView: UIView!
    @IBOutlet weak var coinName: UILabel!
    @IBOutlet weak var coinAlgo: UILabel!
    @IBOutlet weak var detailFooterBackgroundView: UIView!
    
    @IBOutlet weak var hashrateNameLabel: UILabel!
    @IBOutlet weak var hashrateLabel: UILabel!
    
    @IBOutlet weak var workersNameLabel: UILabel!
    @IBOutlet weak var workersLabel: UILabel!
    
    @IBOutlet weak var unpaidNameLabel: UILabel!
    @IBOutlet weak var unpaidLabel: UILabel!
    
    @IBOutlet weak var balanceNameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
  
    @IBAction func batTapped(_ sender: UITapGestureRecognizer) {
        BatActionHelper().batActions()
        angryBatImg.isHidden = !angryBat
    }
    
    @IBAction func backButtonTappedAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func openMenuTappedAction(_ sender: UIButton) {
        self.slideMenuController()?.openRight()
    }
    
    @IBAction func notificationButtonTappedAction(_ sender: UIButton) {
        UNUserNotificationCenter.current().getNotificationSettings { [weak self] (settings) in
            if settings.authorizationStatus == .authorized {
                guard let wallet = self?.wallet, let status = wallet.status else { return }
                var subscribe: Int?
                switch status {
                case .on:
                    subscribe = 0
                case .off:
                    subscribe = 1
                case .unknown:
                    subscribe = 0
                }
                
                self?.service.subscribeNotification(adress: wallet.adress ?? "", subscribe: subscribe ?? 0) { [weak self] error in
                    if let error = error {
                        self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
                    } else {
                        DispatchQueue.main.async {
                            switch status {
                            case .on:
                                self?.wallet?.status = .off
                                self?.notificationButton.setImage(#imageLiteral(resourceName: "Icon Notification Off"), for: .normal)
                            case .off:
                                self?.wallet?.status = .on
                                self?.notificationButton.setImage(#imageLiteral(resourceName: "Icon Notification On"), for: .normal)
                            case .unknown:
                                self?.wallet?.status = .off
                                self?.notificationButton.setImage(#imageLiteral(resourceName: "Icon Notification Off"), for: .normal)
                            }
                            Settings.storage.updateWalletStatus(wallet: wallet, status: self?.wallet?.status ?? .off)
                            NotificationCenter.default.post(name: notificationWalletPushUpdate, object: nil)
                        }
                    }
                }
            }
            else {
                // Either denied or notDetermined
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                    (granted, error) in
                    // add your own
                    let alertController = UIAlertController(title: "NOTIFICATION_ALERT".localized(), message: "NOTIFICATION_ALERT_MESSAGE".localized(), preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "SETTINGS".localized(), style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            })
                        }
                    }
                    let cancelAction = UIAlertAction(title: "CANCEL".localized(), style: .default, handler: { _ in
                        return
                    })
                    alertController.addAction(cancelAction)
                    alertController.addAction(settingsAction)
                    DispatchQueue.main.async {
                        self?.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func subscribeNotification(subscribe: Bool) {
        service.subscribeNotification(adress: wallet?.adress ?? "", subscribe: subscribe ? 1 : 0) { [weak self] error in
            guard let error = error else { return }
            self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
            return
        }
    }
    
    func checkNotificationStatus() {
        guard let status = wallet?.status else { return }
        
        switch status {
        case .on:
            notificationButton.setImage(#imageLiteral(resourceName: "Icon Notification On"), for: .normal)
        case .off:
            notificationButton.setImage(#imageLiteral(resourceName: "Icon Notification Off"), for: .normal)
        case .unknown:
            notificationButton.setImage(#imageLiteral(resourceName: "Icon Notification Off"), for: .normal)
        }
    }
    
    
    @objc func angryBatImage() {
        angryBatImg.isHidden = !angryBat
    }
    
    @objc func changeColors() {
        self.view.backgroundColor = Theme.currentTheme.backgroundColor
        headerBackgroundImage.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "header_dark")! : UIImage(named: "header_light")!
        batImage.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_dark")! : UIImage(named: "bat_light")!
        nameLabel.textColor = Theme.currentTheme.headerTextLabelColor
        mainBackgroundView.backgroundColor = Theme.currentTheme.backgroundColor
        menuButton.setImage(Settings.storage.currentThemeColor == 1 ? UIImage(named: "Icon Menu Dark")! : UIImage(named: "Icon Menu Light")!, for: .normal)
        detailFooterBackgroundView.backgroundColor = Theme.currentTheme.footerBackgroundViewColor
        walletName.textColor = Theme.currentTheme.mainHeaderTextColor
        nextPayoutLabel.textColor = Theme.currentTheme.nextPayoutTextColor
        nextPayoutFromNow.textColor = Theme.currentTheme.nextPayoutTextColor
        hashrateNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
        workersNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
        unpaidNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
        balanceNameLabel.textColor = Theme.currentTheme.mainFooterTextColor
        coinName.textColor = Theme.currentTheme.mainFooterTextColor
        headerBackgroundView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        angryBatImg.image = Settings.storage.currentThemeColor == 1 ? UIImage(named: "bat_angry_dark")! : UIImage(named: "bat_angry_light")!
        firstFooterView.backgroundColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        
        
        infoCollectionView.reloadData()
    }
    
    @objc func changeLanguage() {
        setupWalletData()
        nextPayoutLabel.text = "NEXT_PAYOUT".localized()
        homeButtonLabel.text = "HOME".localized()
        workersButtonLabel.text = "WORKERS_NAME".localized()
        earningsButtonLabel.text = "EARNINGS".localized()
        
        switch selectedButton {
        case 0:
            nameLabel.text = "HOME".localized()
        case 1:
            nameLabel.text = "WORKERS_NAME".localized()
        case 2:
            nameLabel.text = "EARNINGS".localized()
        default:
            nameLabel.text = "HOME".localized()
        }
    }
    
    func setupWalletData() {
        hashrateNameLabel.text = "HASHRATE".localized()
        workersNameLabel.text = "WORKERS_NAME".localized()
        unpaidNameLabel.text = "UNPAID".localized()
        balanceNameLabel.text = "BALANCE".localized()
        
        guard let wallet = wallet else { return }
        guard let walletInfo = wallet.walletInfo else { return }
        walletName.text = wallet.walletName ?? wallet.adress
        
        coinName.text = walletInfo.currency ?? "NoN"
        workersLabel.text = "\(walletInfo.miners?.count ?? 0)"
        unpaidLabel.text = NSString(format: "%.4f", walletInfo.unpaid ?? 0) as String
        balanceLabel.text = NSString(format: "%.4f", walletInfo.balance ?? 0) as String
        if let algo = walletInfo.miners?.first?.algo {
            coinAlgo.text = algo
        }
        else if let algo = walletInfo.last50E?.first?.algo {
            coinAlgo.text = algo
        }
        
        if let hashRate = walletInfo.algorithms?.first?.hashrate {
            hashrateLabel.text = "\(hashRate)"
        } else {
            hashrateLabel.text = "0 Mh/s"
        }
        
        infoCollectionView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        changeColors()
        angryBatImage()
        checkNotificationStatus()
        notifications()
    }
    
    func setup() {
        setupFilterButtons()
        getNextPayout()
        changeLanguage()
        
        infoCollectionView.dataSource = self
        infoCollectionView.register(WorkersCell.self, forCellWithReuseIdentifier: WorkersCell.identifier)
        infoCollectionView.register(HomeCell.self, forCellWithReuseIdentifier: HomeCell.identifier)
        infoCollectionView.register(EarningsCell.self, forCellWithReuseIdentifier: EarningsCell.identifier)
        infoCollectionView.isPagingEnabled = true
    }
    
    func notifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateWalletName), name: notificationWalletName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeColors), name: ThemeChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(angryBatImage), name: BatChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: LCLLanguageChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: WalletArrayRefreshNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func removeBlurredBackgroundView(added: Bool) {
        let subviews = view.subviews.filter { $0.isKind(of: UIVisualEffectView.self) }
        subviews.first?.removeFromSuperview()
    }
    
    func setupFilterButtons() {
        addButtonActions()
        setupButtons()
    }
    
    func addButtonActions() {
        homeButton.addTarget(self, action: #selector(homeButtonAction))
        workersButton.addTarget(self, action: #selector(workersButtonAction))
        earningsButton.addTarget(self, action: #selector(earningsButtonAction))
    }
    
    func setupButtons() {
        switch selectedButton{
        case 0:
            homeButtonImageView.image = #imageLiteral(resourceName: "Home Button Blue")
            workersButtonImageView.image = #imageLiteral(resourceName: "Workers Button White")
            earningsButtonImageView.image = #imageLiteral(resourceName: "Earnings Button White")
            
            homeButtonLabel.textColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
            workersButtonLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            earningsButtonLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case 1:
            homeButtonImageView.image = #imageLiteral(resourceName: "Home Button White")
            workersButtonImageView.image = #imageLiteral(resourceName: "Workers Button Blue")
            earningsButtonImageView.image = #imageLiteral(resourceName: "Earnings Button White")
            
            homeButtonLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            workersButtonLabel.textColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
            earningsButtonLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case 2:
            homeButtonImageView.image = #imageLiteral(resourceName: "Home Button White")
            workersButtonImageView.image = #imageLiteral(resourceName: "Workers Button White")
            earningsButtonImageView.image = #imageLiteral(resourceName: "Earnings Button Blue")
            
            homeButtonLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            workersButtonLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            earningsButtonLabel.textColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.7058823529, alpha: 1)
        default: return
        }
        homeButtonLabel.font = UIFont(name: "Roboto", size: 12)
        workersButtonLabel.font = UIFont(name: "Roboto", size: 12)
        earningsButtonLabel.font = UIFont(name: "Roboto", size: 12)
    }
    
    private func getNextPayout() {
        service.getNextPayout { [weak self] result in
            switch result {
            case .success(let data):
                self?.setupNextPayout(data: data)
            case .error(let error):
                self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
            }
        }
    }
    
    private func setupNextPayout(data: NextPayout) {
        if let intDate = data.nextpayout {
            let date = Date(timeIntervalSince1970: Double(intDate))
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            formatter.locale = .current
            
            let timeToDate = Date().offsetFrom(date: date)
            
            nextPayoutTime.text = formatter.string(from: date)
            nextPayoutFromNow.text = timeToDate
        }
    }
    
    //Actions
    
    @objc func homeButtonAction() {
        selectedButton = 0
        setupButtons()
        nameLabel.text = "HOME".localized()
        infoCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @objc func workersButtonAction() {
        selectedButton = 1
        setupButtons()
        nameLabel.text = "WORKERS_NAME".localized()
        infoCollectionView.scrollToItem(at: IndexPath(row: 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @objc func earningsButtonAction() {
        selectedButton = 2
        setupButtons()
        nameLabel.text = "EARNINGS".localized()
        infoCollectionView.scrollToItem(at: IndexPath(row: 2, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @objc func updateWalletName() {
        self.walletName.text = Settings.storage.getWallet(walletAdress: self.wallet?.adress ?? "").walletName ?? ""
    }
    
    @objc private func reloadData() {
        guard Network.isConnectedToNetwork() else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.showAlert(title: "ERROR".localized(), message: "CONNECTION_ERROR".localized())
            }
            return
        }
        getNextPayout()
        guard let wallet = self.wallet else { return }
        service.search(walletName: wallet.adress ?? "") { [weak self] result in
            switch result {
            case .success(let walletInfo):
                let newWalletModel = WalletModel(status: wallet.status ?? .off, adress: wallet.adress ?? "", walletName: wallet.adress ?? "", walletInfo: walletInfo, customSortIndex: wallet.customSortIndex, isExpanded: wallet.isExpanded)
                Settings.storage.addNewWallet(wallet: newWalletModel)
                self?.wallet = newWalletModel
                self?.setupWalletData()
                NotificationCenter.default.post(name: updateWalletMainScreen, object: nil)
            case .error(let error):
                self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
            }
        }
    }

}

extension WalletDetailViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCell.identifier, for: indexPath) as! HomeCell
            cell.reloadData(wallet ?? WalletModel())
            cell.reloadAction = { [weak self] in
                self?.reloadData()
            }
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WorkersCell.identifier, for: indexPath) as! WorkersCell
            cell.setup(workers: wallet?.walletInfo?.miners ?? [])
            cell.reloadAction = { [weak self] in
                self?.reloadData()
            }
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EarningsCell.identifier, for: indexPath) as! EarningsCell
            cell.setup(last50: wallet?.walletInfo?.last50E ?? [])
            cell.reloadAction = { [weak self] in
                self?.reloadData()
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let indexPath = Int(targetContentOffset.pointee.x)
        switch indexPath {
        case 0:
            selectedButton = 0
            setupButtons()
            nameLabel.text = "HOME".localized()
        case 1...500:
            selectedButton = 1
            setupButtons()
            nameLabel.text = "WORKERS_NAME".localized()
        case 501...1000:
            selectedButton = 2
            setupButtons()
            nameLabel.text = "EARNINGS".localized()
        default:
            return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: infoCollectionView.frame.width, height: infoCollectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension WalletDetailViewController: WalletDetailDelegate {
    func removeBlurredBackgroundView() {
        let subviews = view.subviews.filter { $0.isKind(of: UIVisualEffectView.self) }
        subviews.first?.removeFromSuperview()
    }
    
    func closeDetailScreen() {
        let subviews = view.subviews.filter { $0.isKind(of: UIVisualEffectView.self) }
        subviews.first?.removeFromSuperview()
        slideMenuController()?.navigationController?.popViewController(animated: true)
    }
}
