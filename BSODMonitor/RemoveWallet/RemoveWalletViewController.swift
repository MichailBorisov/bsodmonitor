//
//  RemoveWalletViewController.swift
//  BSODMonitor
//
//  Created by Владислав on 30/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class RemoveWalletViewController: UIViewController {

    @IBOutlet weak var cancelImg: UIImageView!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var cancelBackgroundView: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    @IBOutlet weak var confirmImg: UIImageView!
    @IBOutlet weak var confirmBackgroundView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var secondBodyView: UIView!
    
    var wallet: WalletModel?
    let service = SearchRequestBuilder()
    weak var delegate: WalletDetailDelegate?

    @IBAction func closeAction(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
        delegate?.removeBlurredBackgroundView()
    }

    @IBAction func cancelViewAction(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
        delegate?.removeBlurredBackgroundView()
    }

    @IBAction func confirmViewAction(_ sender: UITapGestureRecognizer) {
        if let wallet = wallet {
            if wallet.status == .on {
                guard Network.isConnectedToNetwork() else {
                    showAlert(title: "ERROR".localized(), message: "NOTIFICATION_CONNECTION_ERROR".localized())
                    return
                }
                service.subscribeNotification(adress: wallet.adress ?? "", subscribe: 0) { [weak self] error in
                    if let error = error {
                        self?.showAlert(title: "ERROR".localized(), message: error.localizedDescription)
                    } else {
                        Settings.storage.removeWallet(wallet: wallet)
                        NotificationCenter.default.post(name: notificationWalletRemoved, object: nil)
                        self?.dismissAction()
                    }
                }
            } else {
                Settings.storage.removeWallet(wallet: wallet)
                NotificationCenter.default.post(name: notificationWalletRemoved, object: nil)
                dismissAction()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        themeChange()
        setText()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: LCLLanguageChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(themeChange), name: ThemeChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func themeChange() {
        bodyView.backgroundColor = Theme.currentTheme.addWalletBackgroundColor
        secondBodyView.backgroundColor = Theme.currentTheme.addWalletBackgroundColor
        nameView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        confirmBackgroundView.backgroundColor = Theme.currentTheme.backgroundWalletStringColor
        cancelBackgroundView.backgroundColor = Theme.currentTheme.cancelButtonColor
        nameLabel.textColor = Theme.currentTheme.walletsNameTextColor
        bodyLabel.textColor = Theme.currentTheme.filterTextColor
    }

    @objc func setText() {
        nameLabel.text = "REMOVE_WALLET".localized()
        bodyLabel.text = "WILLBEREMOVED".localized()
        confirmLabel.text = "CONFIRM".localized()
        cancelLabel.text = "CANCEL".localized()
    }
    
    func dismissAction() {
        dismiss(animated: true, completion: nil)
        delegate?.closeDetailScreen()
    }

}
