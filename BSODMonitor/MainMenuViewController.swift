//
//  ViewController.swift
//  BSODMonitor
//
//  Created by Михаил on 31/10/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Localize_Swift

class MainMenuViewController: UIViewController {

    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var helpBackgroundView: UIView!
    @IBOutlet weak var mainMenuTableView: UITableView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var faqLineView: UIView!

    @IBOutlet weak var faqLabel: UILabel!
    @IBOutlet weak var faqImageView: UIImageView!

    @IBOutlet weak var logoView: UIView!

    @IBAction func faqTapped(_ sender: UIGestureRecognizer) {
        faq()
    }

    var isLight: Bool = true

    var menu: [[MainMenu]] = [
        [MainMenu(name: "BSOD_POOL".localized(), imageLight: UIImage(named: "menu_bsod_light")!, imageDark: UIImage(named: "menu_bsod_dark")!, action: {
            URLOpenHelper().openURL("https://bsod.pw/")
        }),
         MainMenu(name: "TELEGRAM".localized(), imageLight: UIImage(named: "menu_telegram_light")!, imageDark: UIImage(named: "menu_telegram_dark")!, action: {
            URLOpenHelper().openURL("https://t.me/bsodpool")
         }),
         MainMenu(name: "DISCORD".localized(), imageLight: UIImage(named: "menu_discord_light")!, imageDark: UIImage(named: "menu_discord_dark")!, action: {
            URLOpenHelper().openURL("https://discord.gg/uNBBfGM")
         })],
        [MainMenu(name: "NEWS".localized(), imageLight: UIImage(named: "menu_news_light")!, imageDark: UIImage(named: "menu_news_dark")!, action: {
            URLOpenHelper().openURL("https://twitter.com/BsodPool")
        })],
        [MainMenu(name: "BSOD_HELP".localized(), imageLight: UIImage(named: "menu_settings_light")!, imageDark: UIImage(named: "menu_settings_dark")!, action: {})]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        fullUI()
        setupTableView()
        faq()
        setText()
        changeColors()

        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: LCLLanguageChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeColors), name: ThemeChangeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func changeColors() {
        mainMenuTableView.backgroundColor = Theme.currentTheme.menuBackgroundColor
        logoView.backgroundColor = Theme.currentTheme.menuBackgroundColor
        helpBackgroundView.backgroundColor = Theme.currentTheme.menuBackgroundColor
        logoImg.image = Theme.currentTheme.logoImg
        faqLineView.backgroundColor = Theme.currentTheme.linesColor
        self.view.backgroundColor = Theme.currentTheme.menuBackgroundColor
        faqLabel.textColor = Theme.currentTheme.textColor
        faqImageView.image = Settings.storage.currentThemeColor == 0 ? UIImage(named: "menu_info_light")! : UIImage(named: "menu_info_dark")!
        
        tableView.reloadData()
    }
    
    @IBAction func settingsAction(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        guard let subContentsVC = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else { return }
        present(subContentsVC, animated: true, completion: nil)
        self.slideMenuController()?.closeRight()
    }

    @objc func setText() {
        menu[1][0].name = "NEWS".localized()
        menu[2][0].name = "SETTINGS".localized()
        faqLabel.text = "BSOD_HELP".localized()

        tableView.reloadData()
    }

    private func fullUI() {
        versionLabel.text = "ver. \(ConfigHelper.Application.versionNumber)(\(ConfigHelper.Application.buildNumber))"
        versionLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }

    private func setupTableView() {
        tableView.isScrollEnabled = false
        tableView.tableFooterView = UIView(frame: CGRect.zero)

    }

    private func openSettings() {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController")
        let navC = UINavigationController(rootViewController: vc)
        navC.isNavigationBarHidden = true
        self.slideMenuController()?.closeRight()
        self.slideMenuController()?.show(navC, sender: nil)
    }

    private func faq() {
        let storyboard = UIStoryboard(name: "SupportPage", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "SupportPageViewController") as? SupportPageViewController else { return }
        let navC = UINavigationController(rootViewController: vc)
        navC.isNavigationBarHidden = true
        self.slideMenuController()?.closeRight()
        self.slideMenuController()?.show(navC, sender: nil)
    }

}

extension MainMenuViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return menu.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? MainMenuTableViewCell else { return UITableViewCell() }

        cell.nameLabel.text = menu[indexPath.section][indexPath.row].name
        cell.thumbnailImageView.image = Settings.storage.currentThemeColor == 0 ? menu[indexPath.section][indexPath.row].imageLight : menu[indexPath.section][indexPath.row].imageDark
        cell.nameLabel.textColor = Theme.currentTheme.textColor
        cell.selectionStyle = .none

        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let lineView = UIView()
        let viewInView = UIView(frame: CGRect(x: lineView.frame.origin.x + 20, y: lineView.frame.origin.y, width: view.frame.width - 40, height: 1))
        viewInView.backgroundColor = Theme.currentTheme.linesColor
        lineView.addSubview(viewInView)

        return lineView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == IndexPath(row: 0, section: 2) {
            self.openSettings()
        }
        else {
            menu[indexPath.section][indexPath.row].action()
        }
    }
}
