//
//  StoreSettings.swift
//  BSODMonitor
//
//  Created by Михаил on 06/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

final class Settings {

    private let userWalletsKey = "userWalletsKey"
    private let themeColorKey = "themeColorKey"
    private let choosedIndex = "choosedIndex"
    private let confirmationDeleteKey = "confirmationDeleteKey"
    private let deviceToken = "deviceToken"

    static private var instance: Settings! = nil

    static var storage: Settings {
        get {
            if Settings.instance == nil {
                Settings.instance = Settings()
            }

            return Settings.instance
        }
    }

    private init() {}

    private func set(_ walletArray: [WalletModel]) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(walletArray), forKey: userWalletsKey)
        UserDefaults.standard.synchronize()
    }

    private func get() -> [WalletModel]? {
        if let data = UserDefaults.standard.value(forKey: userWalletsKey) as? Data {
            return try? PropertyListDecoder().decode([WalletModel].self, from: data)
        } else {
            return nil
        }
    }

    // global variable

    var currentThemeColor: Int {
        //light 0
        //dark 1
        get {
            if let color = UserDefaults.standard.object(forKey: themeColorKey) as? Int {
                return color
            }
            else {
                UserDefaults.standard.set(0, forKey: themeColorKey)
                return 0
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: themeColorKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    var token: String {
        get {
            if let index = UserDefaults.standard.object(forKey: deviceToken) as? String {
                return index
            }
            else {
                UserDefaults.standard.set("", forKey: deviceToken)
                return ""
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: deviceToken)
            UserDefaults.standard.synchronize()
        }
    }
    
    var sortChoosed: Int {
        get {
            if let index = UserDefaults.standard.object(forKey: choosedIndex) as? Int {
                return index
            }
            else {
                UserDefaults.standard.set(0, forKey: choosedIndex)
                return 0
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: choosedIndex)
            UserDefaults.standard.synchronize()
        }
    }

    var confirmationDelete: Bool {
        get {
            if let confirmation = UserDefaults.standard.object(forKey: confirmationDeleteKey) as? Bool {
                return confirmation
            }
            else {
                UserDefaults.standard.set(true, forKey: confirmationDeleteKey)
                return true
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: confirmationDeleteKey)
            UserDefaults.standard.synchronize()
        }
    }

    // MARK: - Wallets

    func createWalletsArray() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode([WalletModel]()), forKey: userWalletsKey)
    }

    func getWalletsArray() -> [WalletModel] {
        if let wallets = get() {
            return wallets
        }
        else {
            createWalletsArray()
            return [WalletModel]()
        }
    }

    func getWallet(walletAdress: String) -> WalletModel {
        guard var wallets = get() else { fatalError("Кошелька не существует") }

        if let index = wallets.firstIndex(where: { $0.adress == walletAdress }) {
            return wallets[index]
        }
        else {
            fatalError("Кошелька не существует")
        }
    }
    
    func updateWalletIndex(wallet: WalletModel, newIndex: Int) {
        guard var wallets = get() else { return }
        
        if let walletIndex = wallets.firstIndex(where: { $0.adress == wallet.adress }) {
            wallets[walletIndex].customSortIndex = newIndex
        }
        UserDefaults.standard.synchronize()
        set(wallets)
    }
    
    func updateWalletIsExpandedStatus(wallet: WalletModel, isExpanded: Bool) {
        guard var wallets = get() else { return }
        
        if let walletIndex = wallets.firstIndex(where: { $0.adress == wallet.adress }) {
            wallets[walletIndex].isExpanded = isExpanded
        }
        UserDefaults.standard.synchronize()
        set(wallets)
    }

    func updateWallet(wallet: Wallet, walletAdress: String) {
        guard var wallets = get() else { return }

        if let index = wallets.firstIndex(where: { $0.adress == walletAdress }) {
            wallets[index].walletInfo = wallet
        }

        set(wallets)
    }
    
    func updateWalletStatus(wallet: WalletModel, status: NotificationStatus) {
        guard var wallets = get() else { return }
        
        if let index = wallets.firstIndex(where: { $0.adress == wallet.adress }) {
            wallets[index].status = status
        }
        
        set(wallets)
    }

    func addNewWallet(wallet: WalletModel) {
        guard var wallets = get() else { return }

        if !wallets.contains(where: { $0.adress == wallet.adress }) {
            wallets.append(wallet)
        }

        set(wallets)
    }

    func changeWalletName(wallet: WalletModel, newName: String) {
        guard var wallets = get() else { return }

        if let walletIndex = wallets.firstIndex(where: { $0.adress == wallet.adress }) {
            wallets[walletIndex].walletName = newName
        }
        UserDefaults.standard.synchronize()
        set(wallets)
    }

    func removeWallet(wallet: WalletModel) {
        guard var wallets = get() else { return }

        if let walletIndex = wallets.firstIndex(where: { $0.adress == wallet.adress }) {
            wallets.remove(at: walletIndex)
        }

        set(wallets)
    }
}
