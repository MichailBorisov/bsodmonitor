//
//  FilterArrowCell.swift
//  BSODMonitor
//
//  Created by Михаил on 22/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class FilterArrowCell: UICollectionViewCell {
    @IBOutlet weak var filterImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        filterImageView.image = Theme.currentTheme.arrayImg
    }

    override var isSelected: Bool {
        didSet {
            isSelected ? (filterImageView.image = Theme.currentTheme.arrayImg) : (filterImageView.image = Theme.currentTheme.arrayImg)
        }
    }

}
