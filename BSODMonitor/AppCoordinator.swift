//
//  AppCoordinator.swift
//  BSODMonitor
//
//  Created by Михаил on 16/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class AppCoordinator {

    public var isLight: Bool = true

    private var window: UIWindow

    private var rootController: UIViewController! {
        didSet {
            self.window.rootViewController = self.rootController
            self.window.makeKeyAndVisible()
        }
    }

    init(window: UIWindow) {
        self.window = window
        self.window.frame = UIScreen.main.bounds
    }

    func start() {
        self.instanceMainController()
    }
}

extension AppCoordinator {

    private func instanceMainController() {
        if Application.isAppAlreadyLaunchedOnce() {
            pushMainViewController()
        }
        else {
            Settings.storage.createWalletsArray()
            pushTutorialViewController()
        }
    }

    private func pushTutorialViewController() {
        let swipingStoryboard = UIStoryboard(name: "Tutorial", bundle: nil)
        let swipingController = swipingStoryboard.instantiateViewController(withIdentifier: "TutorialViewController")
        self.rootController = swipingController
    }

    private func pushMainViewController() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController")

        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let menuViewController = menuStoryboard.instantiateViewController(withIdentifier: "MainMenuViewController")

        let navC = UINavigationController(rootViewController: mainViewController)
        navC.setNavigationBarHidden(true, animated: true)

        let slideMenuController = SlideMenuController(mainViewController: navC, rightMenuViewController: menuViewController)
        slideMenuController.changeRightViewWidth(self.window.bounds.width - 60)

        self.rootController = slideMenuController
    }

}
