//
//  CollectionViewExtension.swift
//  BSODMonitor
//
//  Created by Михаил on 15/12/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

extension UICollectionViewCell {

    class var identifier: String {
        return String(describing: self)
    }

    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}
