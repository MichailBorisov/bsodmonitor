//
//  TableViewCellExtension.swift
//  BSODMonitor
//
//  Created by Михаил on 15/12/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

extension UITableViewCell {

    class var identifier: String {
        return String(describing: self)
    }

    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}
