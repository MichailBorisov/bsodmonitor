//
//  RequestService.swift
//  BSODMonitor
//
//  Created by Михаил on 25/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import Alamofire

final class RequestService {

    enum Result {
        case success(Data)
        case failure(Error)
    }

    struct WalletError: Codable {
        let error: String
    }

    func process(url: URLConvertible, method: HTTPMethod, parameters: Parameters?, headers: HTTPHeaders?, completionHandler: @escaping (Result) -> Void) {

        Alamofire.request(url, method: method, parameters: parameters, headers: headers).responseData { response in

            if let err = response.result.error {
                completionHandler(.failure(err))
                return
            }

            if let result = response.result.value {
                do {
                    let searchResult = try JSONDecoder().decode(WalletError.self, from: result)

                    completionHandler(.failure(NSError(domain: "BSOD Error", code: 0, userInfo: [NSLocalizedDescriptionKey: searchResult.error])))
                }
                catch {
                    completionHandler(.success(result))
                }
            }
        }
    }
}
