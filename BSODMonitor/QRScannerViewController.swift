//
//  QRScannerViewController.swift
//  BSODMonitor
//
//  Created by Владислав on 09/12/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit
import AVFoundation

class QRScannerViewController: UIViewController {

    lazy var captureSession  = AVCaptureSession()
    weak var delegate: QRScannerDelegate?

    let authStatus = AVCaptureDevice.authorizationStatus(for: .video)

    private lazy var dismissButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "Icon Tutorial Dismiss"), for: .normal)
        button.addTarget(self, action: #selector(dismissAction), for: .touchUpInside)
        return button
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "Icon Scan QR")
        return imageView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        QRScannerSettings()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession.isRunning == false) {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    private func QRScannerSettings() {
        view.backgroundColor = UIColor.black

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        }
        catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        }
        else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        }
        else {
            failed()
            return
        }

        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        view.addSubview(dismissButton)
        NSLayoutConstraint.activate([
            dismissButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40),
            dismissButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            dismissButton.widthAnchor.constraint(equalToConstant: 40)
            ])
        dismissButton.addConstraint(NSLayoutConstraint(item: dismissButton, attribute: .height, relatedBy: .equal, toItem: dismissButton, attribute: .width, multiplier: 1, constant: 0))

        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 300)
            ])
        imageView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: imageView, attribute: .width, multiplier: 1, constant: 0))

        captureSession.startRunning()
    }

    private func failed() {
        showAlert(title: "SCANNING_NOT_SUPPORTED".localized(), message: "SCANNING_NOT_SUPPORTED_DESCRIPTION".localized())
        dismiss(animated: true, completion: nil)
    }

    @objc private func dismissAction() {
        captureSession.stopRunning()
        dismiss(animated: true, completion: nil)
    }

}

extension QRScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                        
            delegate?.didScan(walletName: stringValue.replacingOccurrences(of: APIconstant.openBSODUrl, with: ""))
        }
        dismiss(animated: true)
    }
}
