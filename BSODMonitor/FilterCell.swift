//
//  FilterCell.swift
//  BSODMonitor
//
//  Created by Михаил on 13/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class FilterCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var filterImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        filterImageView.image = Theme.currentTheme.linesImage
        nameLabel.textColor = Theme.currentTheme.filterTextColor
    }

    override var isSelected: Bool {
        didSet {
            isSelected ? (nameLabel.textColor = Theme.currentTheme.filterTextSelected) : (nameLabel.textColor = Theme.currentTheme.filterTextColor)
            isSelected ? (filterImageView.image = #imageLiteral(resourceName: "Icon Sort Lines Blue")) : (filterImageView.image = Theme.currentTheme.linesImage)
        }
    }
}
