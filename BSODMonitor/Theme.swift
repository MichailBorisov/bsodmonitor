//
//  Theme.swift
//  BSODMonitor
//
//  Created by Владислав on 22/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class Theme {
    static var currentTheme: ThemeProtocol = Settings.storage.currentThemeColor == 0 ? LightTheme() : DarkTheme()
}
