//
//  IfAlreadyLaunched.swift
//  BSODMonitor
//
//  Created by Михаил on 15/12/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import Foundation

final class Application {

    static func isAppAlreadyLaunchedOnce() -> Bool {
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "isAppAlreadyLaunchedOnce") != nil {
            return true
        }
        else {
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            return false
        }
    }

}
