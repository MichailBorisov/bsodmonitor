//
//  ThemeProtocol.swift
//  BSODMonitor
//
//  Created by Владислав on 22/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

protocol ThemeProtocol {
    var backgroundColor: UIColor { get }
    var textColor: UIColor { get }
    var sortBackgroundColor: UIColor { get }
    var arrayImg: UIImage? { get }
    var linesColor: UIColor { get }
    var filterTextColor: UIColor { get }
    var filterTextSelected: UIColor { get }
    var linesImage: UIImage? { get }
    var backgroundWalletStringColor: UIColor { get }
    var addWalletBackgroundColor: UIColor { get }
    var menuBackgroundColor: UIColor { get }
    var logoImg: UIImage? { get }
    var bsodImg: UIImage? { get }
    var telegramImg: UIImage? { get }
    var discordImg: UIImage? { get }
    var newsImg: UIImage? { get }
    var settingsImg: UIImage? { get }
    var faqImg: UIImage? { get }
    var placeHolderTextColor: UIColor { get }
    var nextPayoutTextColor: UIColor { get }
    var headerTextLabelColor: UIColor { get }
    var cancelButtonColor: UIColor { get }
    var walletsNameTextColor: UIColor { get }
    var keyboardTheme: UIKeyboardAppearance { get }
    var footerBackgroundViewColor: UIColor { get }
    var mainHeaderTextColor: UIColor { get }
    var mainFooterTextColor: UIColor { get }
    var detailsViewBackgroundColor: UIColor { get }
    var workersFooterBackgroundViewColor: UIColor { get }
    var detailsTextColor: UIColor { get }
    var workersNameLabelColor: UIColor { get }
    var unevenTableViewCellsBackground: UIColor { get }
    var evenTableViewCellsBackground: UIColor { get }
    var homeCellTextColor: UIColor { get }
    var amountTextColor: UIColor { get }
    var footerLineColor: UIColor { get }
}
