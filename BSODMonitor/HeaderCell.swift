//
//  MainViewCell.swift
//  BSODMonitor
//
//  Created by Михаил on 11/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var openDetailScreenButton: UIButton!
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var wallenNameLabel: UILabel!

    var openDetailScreen: () -> Void = {}
    var openDetailCell: () -> Void = {}

    override func layoutSubviews() {
        super.layoutSubviews()
        statusView.layer.cornerRadius = statusView.frame.size.width/2
    }

    @IBAction func openButtonTappedAction(_ sender: UIButton) {
        openDetailCell()
    }

    @IBAction func openDetailScreenButtonTapped(_ sender: UIButton) {
        openDetailScreen()
    }

}
