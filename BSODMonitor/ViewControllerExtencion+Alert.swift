//
//  ViewControllerExtencion+Alert.swift
//  BSODMonitor
//
//  Created by Михаил on 06/01/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

extension UIViewController {

    var isModal: Bool {
        return presentingViewController != nil ||
            navigationController?.presentingViewController?.presentedViewController === navigationController ||
            tabBarController?.presentingViewController is UITabBarController
    }

    func showAlert(title: String?, message: String, dismiss: Bool = false, presented: Bool = false) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "CLOSE".localized(), style: .default, handler: { (_: UIAlertAction!) in

            if dismiss {
                if presented {
                    self.dismiss(animated: true)
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }))
        isModal ? show(alert, sender: nil) : present(alert, animated: true)
    }

    func showComingSoonMessage() {
        let ac = UIAlertController(title: "Coming soon...", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)

        ac.addAction(okAction)

        isModal ? show(ac, sender: nil) : present(ac, animated: true)
    }
    
    func overlayBlurredBackgroundView() {
        let blurredBackgroundView = UIVisualEffectView()
        
        blurredBackgroundView.frame = view.frame
        blurredBackgroundView.alpha = 0.5
        blurredBackgroundView.effect = UIBlurEffect(style: .dark)
        
        view.addSubview(blurredBackgroundView)
    }
}
