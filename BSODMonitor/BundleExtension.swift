//
//  BundleExtension.swift
//  BSODMonitor
//
//  Created by Михаил on 15/12/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? "no version"
    }
    var buildVersionNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? "no version"
    }
}
