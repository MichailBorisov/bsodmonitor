//
//  TutorialCell.swift
//  BSODMonitor
//
//  Created by Михаил on 16/11/2018.
//  Copyright © 2018 Михаил. All rights reserved.
//

import UIKit

class TutorialCell: UICollectionViewCell {

    let imageView: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleToFill
        if #available(iOS 11.0, *) {
            v.adjustsImageSizeForAccessibilityContentSizeCategory = true
        } else {
            // Fallback on earlier versions
        }
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    private func setupLayout() {
        self.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
